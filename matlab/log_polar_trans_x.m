function [ B ] = log_polar_trans_x( A )
	
	[h,w,d] = size(A);
	cx = w/2;
	cy = h/2;
	cz = d/2;
	
	theta = linspace(0, 2*pi, d+1);
	theta(end) = [];
	
	rho = logspace(0, log10(min([h-cy cy-1 d-cz cz-1])), h)';
	
	xx = zeros(h,w,d);
	yy = zeros(h,w,d);
	zz = zeros(h,w,d);
	for i = 1:w
		xx(:,i,:) = i;
		yy(:,i,:) = rho*cos(theta) + cy;
		zz(:,i,:) = rho*sin(theta) + cz;
	end
	
	B = interp3(A, xx, yy, zz, 'linear');
	
	mask = (yy>h) | (yy<1) | (zz>d) | (zz<1);
	B(mask) = 0;

end

