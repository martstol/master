function [ ] = printDicomData( directory )

if (~exist(directory, 'dir'))
	error(strcat('Directory ', directory, ' does not exist or is not a directory'));
end

DCM_ENDING = '.dcm';

files = dir(directory);
n = length(files);
count = 0;
for i = 1:n
	f = files(i);
	if (~f.isdir && strendswith(f.name, DCM_ENDING))
		count = count + 1;
		if (count ~= 0)
			info=dicominfo(strcat(directory, f.name));
		end
	end
end

fprintf('[%i, %i, %i], %s\n', info.Width, info.Height, count, info.FileModDate);

end

