function [ ] = print_volume( I, out_path )

for slice = 1:10:size(I, 3)
	
	imshow(I(:,:,slice));
	print(strcat(out_path, '/', 'z_plane', '/', num2str(slice), '.png'), '-dpng');
	
end

I1 = rotateVolume(I, 90, 'x');

for slice = 1:10:size(I1, 3)
	
	imshow(I1(:,:,slice));
	print(strcat(out_path, '/', 'y_plane', '/', num2str(slice), '.png'), '-dpng');
	
end

I1 = rotateVolume(I, 90, 'y');

for slice = 1:10:size(I1, 3)
	
	imshow(I1(:,:,slice));
	print(strcat(out_path, '/', 'x_plane', '/', num2str(slice), '.png'), '-dpng');
	
end
