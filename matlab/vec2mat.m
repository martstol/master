function [ mat ] = vec2mat( vec, m, n)
	assert(length(vec) == m*n);
	
	mat = zeros(m,n);
	
	for i = 1:m
		for j = 1:n
			index = j + (i-1)*n;
			mat(i,j)=vec(index);
		end
	end

end

