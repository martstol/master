function [ filter ] = make_highpass_filter( h, w, d )

	filter = 1 - make_lowpass_filter(h, w, d);

end

