drive = '/media/martin/My Passport/';
R1 = 'StoneCollection_XCT_Round01_20140820/';
S1 = '20140826_XCT_Nidaros_Good_Large_01/OTB20140826_NidarosGoodLarge1_01/NidarosGL1-dicom/';
folder = strcat(drive, R1, S1);
freq = 10;
I = loadDicomVolume(folder, freq);
angles = [0, 30, 45, 60, 90, 120, 135, 150, 180, 210, 225, 240, 270, 300, 315, 330];
axis = 'z';
for angle = angles
	J = rotateVolume(I, angle, axis);
	[PI, PJ] = padVolumes(I, J);
	estimate0 = mod(estimate_rotation(PI, PJ, axis), 360);
	estimate1 = mod(estimate0 + 90, 360);
	estimate2 = mod(estimate0 + 180, 360);
	estimate3 = mod(estimate0 + 270, 360);
	fprintf('truth: %.2f, estimate0: %.2f, estimate1: %.2f, estimate2: %.2f, estimate3: %.2f\n', ...
		angle, estimate0, estimate1, estimate2, estimate3);
end