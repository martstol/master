function [ ] = print_volumes( I, J, method, out_path )

for slice = 1:10:min(size(I, 3), size(J, 3))
	
	imshowpair(I(:,:,slice), J(:,:,slice), method, 'Scaling', 'none');
	print(strcat(out_path, '/', method, '/', 'z_plane', '/', num2str(slice), '.png'), '-dpng');
	
end

I1 = rotateVolume(I, 90, 'x');
J1 = rotateVolume(J, 90, 'x');

for slice = 1:10:min(size(I1, 3), size(J1, 3))
	
	imshowpair(I1(:,:,slice), J1(:,:,slice), method, 'Scaling', 'none');
	print(strcat(out_path, '/', method, '/', 'y_plane', '/', num2str(slice), '.png'), '-dpng');
	
end

I1 = rotateVolume(I, 90, 'y');
J1 = rotateVolume(J, 90, 'y');

for slice = 1:10:min(size(I1, 3), size(J1, 3))
	
	imshowpair(I1(:,:,slice), J1(:,:,slice), method, 'Scaling', 'none');
	print(strcat(out_path, '/', method, '/', 'x_plane', '/', num2str(slice), '.png'), '-dpng');
	
end
