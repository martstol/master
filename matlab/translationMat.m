function [ m ] = translationMat( v )
	assert(length(v) == 3);
	m = [
		1 0 0 v(1);
		0 1 0 v(2);
		0 0 1 v(3);
		0 0 0 1;
	];
end

