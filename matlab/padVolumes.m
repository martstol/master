function [ OI, OJ ] = padVolumes( I, J )

	[hi,wi,di] = size(I);
	[hj,wj,dj] = size(J);
	pi = hj-hi;
	pj = wj-wi;
	pk = dj-di;
	OI = padarray(I, [max(pi,0), max(pj,0), max(pk,0)], 0, 'post');
	OJ = padarray(J, [max(-pi,0), max(-pj,0), max(-pk,0)], 0, 'post');

end

