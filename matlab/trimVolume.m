function [ J ] = trimVolume( I )
	
	h = 0;
	w = 0;
	d = 0;
	
	i0 = 0;
	j0 = 0;
	k0 = 0;

	for i = 1:size(I,3)
		a = any(any(I(:,:,i)));
		if k0 == 0 && a ~= 0
			k0 = i;
		end
		d = d + a;
	end
	for i = 1:size(I,2)
		a = any(any(I(:,i,:)));
		if j0 == 0 && a ~= 0
			j0 = i;
		end
		w = w + a;
	end
	for i = 1:size(I,1)
		a = any(any(I(i,:,:)));
		if i0 == 0 && a ~= 0
			i0 = i;
		end
		h = h + a;
	end
	
	J = zeros(h,w,d);
	for i = 1:h
		for j = 1:w
			for k = 1:d
				J(i,j,k) = I(i+i0,j+j0,k+k0);
			end
		end
	end
	
end

