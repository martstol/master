function [ theta ] = find_best_rotation( I, J, angle, axis )

	assert(strcmp(axis, 'z') || strcmp(axis, 'y') || strcmp(axis, 'x'), ...
		'Invalid axis. Valid values are ''x'', ''y'' or ''z''.');
	
	threshold = 0.01;
	
	bestAngle = 0;
	bestCmp = 0;
	angles = [0 90 180 270];
	for a = angles
		K = rotateVolume( J, -angle + a, axis );
		[PI, PK] = padVolumes(I, K);
		cmp = error_metric(PI, PK);
		if cmp > bestCmp && cmp >= threshold
			bestAngle = -(-angle + a);
			bestCmp = cmp;
		end
	end
	
	theta = bestAngle;

end

