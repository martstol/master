function [ J ] = rotateVolume( I, angle, axis )
	
	assert(strcmp(axis, 'z') || strcmp(axis, 'y') || strcmp(axis, 'x'), ...
		'Invalid axis. Valid values are ''x'', ''y'' or ''z''.');
	
	if (strcmp(axis, 'z'))
		tform = affine3d([cosd(angle) -sind(angle) 0 0; sind(angle) cosd(angle) 0 0; 0 0 1 0; 0 0 0 1]);
	elseif (strcmp(axis, 'y'))
		tform = affine3d([cosd(angle) 0 -sind(angle) 0; 0 1 0 0; sind(angle) 0 cosd(angle) 0; 0 0 0 1]);
	elseif (strcmp(axis, 'x'))
		tform = affine3d([1 0 0 0; 0 cosd(angle) -sind(angle) 0; 0 sind(angle) cosd(angle) 0; 0 0 0 1]);
	end
	
	J = imwarp(I, tform, 'linear');
	
end

