function [ i, j, k, value ] = phase_correlation( I, J )
	
	FI = fftn(I);
	FJ = fftn(J);
	
	C = cross_power_spectrum(FI, FJ);
	c = real(ifftn(C));
	
	% Find the peak of the phase correlation
	[i,j,k,value] = find_max(c);
	
	i = i - 1;
	j = j - 1;
	k = k - 1;

end

