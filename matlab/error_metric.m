function [ e ] = error_metric( I, J )
% Error metric based on phase correlation. Used for measuring how well
% the two volumes orientation align. 1 means no error. Smaller values means
% larger error.
	
	[~,~,~,e] = phase_correlation(I, J);

end

