function [ ax, ay, az, tx, ty, tz ] = voxel_data_registration( I, J )
	
	%Ensure I and J has the same size
	[Padded_I, Padded_J] = padVolumes(I, J);
	az = estimate_rotation(Padded_I, Padded_J, 'z');
	az = find_best_rotation(I, J, az, 'z');
	if (az ~= 0)
		J = rotateVolume(J, -az, 'z');
	end
	
	[Padded_I, Padded_J] = padVolumes(I, J);
	ay = estimate_rotation(Padded_I, Padded_J, 'y');
	ay = find_best_rotation(I, J, ay, 'y');
	if (ay ~= 0)
		J = rotateVolume(J, -ay, 'y');
	end
	
	[Padded_I, Padded_J] = padVolumes(I, J);
	ax = estimate_rotation(Padded_I, Padded_J, 'x');
	ax = find_best_rotation(I, J, ax, 'x');
	if (az ~= 0)
		J = rotateVolume(J, -ax, 'x');
	end
	
	[Padded_I, Padded_J] = padVolumes(I, J);
	[i,j,k] = phase_correlation(Padded_I, Padded_J);
	
	i = i-1;
	j = j-1;
	k = k-1;
	
	[h,w,d] = size(Padded_I);
	if (i > (h / 2))
		ty = i - h;
	else
		ty = i;
	end
	if (j > (w / 2))
		tx = j - w;
	else
		tx = j;
	end
	if (k > (d / 2))
		tz = k - d;
	else
		tz = k;
	end
	
end

