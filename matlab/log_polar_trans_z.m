function [ B ] = log_polar_trans_z( A )

	[h,w,d] = size(A);
	cx = w/2;
	cy = h/2;
	cz = d/2;
	
	theta = linspace(0, 2*pi, w+1);
	theta(end) = [];
	
	rho = logspace(0, log10(min([w-cx cx-1 h-cy cy-1])), h)';
	
	xx = zeros(h,w,d);
	yy = zeros(h,w,d);
	zz = zeros(h,w,d);
	for i = 1:d
		xx(:,:,i) = rho*cos(theta) + cx;
		yy(:,:,i) = rho*sin(theta) + cy;
		zz(:,:,i) = i;
	end
	
	B = interp3(A, xx, yy, zz, 'linear');
	
	mask = (xx>w) | (xx<1) | (yy>h) | (yy<1);
	B(mask) = 0;
end

