function [i,j,k] = max_coord( A )
%Returns the coorinates of the maximum value of a matrix

[i,j,k] = ind2sub(size(A), (find(A == max(max(max(A))))));

end

