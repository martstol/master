function [ C ] = cross_power_spectrum( I, J )
	C = I .* conj(J);
	C = C ./ abs(C);
	C(isnan(C)) = 0;
end

