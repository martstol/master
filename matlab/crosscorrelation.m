function [ cc ] = crosscorrelation( I, J )

	cc = sum(sum(I .* J)) / sum(sum(I .* I));

end

