function [ e ] = meanSquareError( x, y )
assert(length(x) == length(y));
e = (1 / length(x)) * sum((x-y).^2);
end

