function [ filter ] = make_lowpass_filter( h, w, d )

	dx = 1 / (w-1);
	dy = 1 / (h-1);
	dz = 1 / (d-1);
	
	a = (cos(2*pi*(-0.5:dx:0.5)) + 1) * 0.5;
	b = (cos(2*pi*(-0.5:dy:0.5)) + 1) * 0.5;
	c = (cos(2*pi*(-0.5:dz:0.5)) + 1) * 0.5;
	
	I = b'*a;
	filter = zeros(h,w,d);
	for i=1:d
		filter(:,:,i) = c(i) * I;
	end

end

