function [ errors ] = mseAverage( transforms, registered, sizes, downscale )
	n = size(registered, 1);
	m = size(transforms, 1);
	
	assert(mod(n, m) == 0);
	assert(size(registered, 2) == size(transforms, 2));

	errors = zeros(m, 1);
	
	for i = 1:n
		j = mod(i-1,m)+1;
		
		center = 0.5 * (sizes(floor(i/n)+1,:)/downscale);
		c0 = translationMat(-center);
		c1 = translationMat(center);
		groundTruth = mat2vec((c1*vec2mat(transforms(j,:), 4, 4)'*c0)');
		
		errors(j) = errors(j) + meanSquareError(registered(i,:), groundTruth);
	end
	
end

