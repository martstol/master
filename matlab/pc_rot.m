directory = '/home/martin/Documents/R02_NidarosGoodLarge02_dicom/';
sample = 10;
I = loadDicomVolume(directory, sample);
I = padarray(I, [64,64,64], 0);
J = loadDicomVolume(directory, sample);
J = padarray(J, [64,64,64], 0);

a = 45;
rx = affine3d([1 0 0 0; 0 cosd(a) -sind(a) 0; 0 sind(a) cosd(a) 0; 0 0 0 1]);
ry = affine3d([cosd(a) 0 -sind(a) 0; 0 1 0 0; sind(a) 0 cosd(a) 0; 0 0 0 1]);
rz = affine3d([cosd(a) -sind(a) 0 0; sind(a) cosd(a) 0 0; 0 0 1 0; 0 0 0 1]);

J = imwarp(J, rx);
[hi,wi,di] = size(I);
[hj,wj,dj] = size(J);
pi = hj-hi;
pj = wj-wi;
pk = dj-di;
I = padarray(I, [max(pi,0), max(pj,0), max(pk,0)], 0, 'post');
J = padarray(J, [max(-pi,0), max(-pj,0), max(-pk,0)], 0, 'post');
fprintf('Data initialized\n');

assert(size(I, 1) == size(J, 1) && size(I, 2) == size(J, 2) && size(I, 3) == size(J, 3), 'Image dimensions does not match');

FI = fftshift(fftn(I));
FJ = fftshift(fftn(J));
fprintf('Images Fourier transformed\n');

[h,w,d] = size(FI);
HPF = make_highpass_filter(h,w,d);
LI = log_polar_trans_z(HPF .* abs(FI));
LJ = log_polar_trans_z(HPF .* abs(FJ));
fprintf('Cylindrical coordinates calculated\n');

THETA_I = fftn(LI);
THETA_J = fftn(LJ);

a1 = angle(THETA_I);
a2 = angle(THETA_J);

THETA_CROSS = exp(1i * (a1 - a2));
THETA_PHASE = real(ifftn(THETA_CROSS));
fprintf('Phase correlation complete\n');

[i,j,k] = max_coord(THETA_PHASE);

%dpp = 360 / w;
%theta = dpp * (j - 1);
