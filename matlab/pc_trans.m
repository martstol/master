% I = imread('./fourier_mellin/lena_cropped.bmp');
% J = imread('./fourier_mellin/lena_cropped_shifted.bmp');

directory = '/home/martin/Documents/R02_NidarosGoodLarge02_dicom/';
sample = 10;

I = loadDicomVolume(directory, sample);
I = padarray(I, [64,64,64], 0);
J = loadDicomVolume(directory, sample);
J = padarray(J, [64,64,64], 0);

J = imtranslate(J, [32, 24, 16]);

FI = fftn(I);
FJ = fftn(J);
T = FI .* conj(FJ);
R = T ./ abs(T);
E = abs(ifftn(R));
[i,j,k,value] = find_max(E);

disp(value);

Ty = i - 1;
Tx = j - 1;
Tz = k - 1;

if (i > (size(I, 1) / 2))
	Ty = Ty - size(I, 1);
end

if (j > (size(I, 2) / 2))
	Tx = Tx - size(I, 2);
end

if (k > (size(I, 3) / 2))
	Tz = Tz - size(I, 3);
end
