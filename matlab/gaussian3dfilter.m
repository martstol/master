function [ filter ] = gaussian3dfilter( w, h, d, s )

assert(mod(w, 2) == 1 && mod(h, 2) == 1 && mod(d, 2) == 1, ...
		'All filter dimensions must have odd size');
assert(s > 0, 'Sigma must be positive and greater than 0');

filter = zeros(h,w,d);

gaussian3d = @(x,y,z) exp(-(x*x+y*y+z*z)/(2*s*s));
cx = (w-1)/2;
cy = (h-1)/2;
cz = (d-1)/2;
nGaussian3d = @(x,y,z) gaussian3d(x/cx, y/cy, z/cz);
cnGaussian3d = @(x,y,z) nGaussian3d(x-cx, y-cy, z-cz);

for k = 1:d
    for j = 1:w
        for i = 1:h
            filter(i,j,k) = cnGaussian3d(j-1,i-1,k-1);
        end
    end
end

filter = filter ./ sum(sum(sum(filter)));

end

