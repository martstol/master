function volume = loadDicomVolume(directory, frequency)
% Create a 3D volume from all .dcm files in a directory and downscale it

if (~exist(directory, 'dir'))
	error(strcat('Directory ', directory, ' does not exist or is not a directory'));
end

DCM_ENDING = '.dcm';

files = dir(directory);
n = length(files);
volume = uint16([]);
count = 0;
for i = 1:frequency:n
	f = files(i);
	if (~f.isdir && strendswith(f.name, DCM_ENDING))
		count = count + 1;
		slice = dicomread(strcat(directory, f.name));
		down = imresize(slice, 1 / frequency, 'bilinear');
		volume(:,:,count) = down;
	end
end
