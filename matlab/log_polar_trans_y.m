
function [ B ] = log_polar_trans_y( A )

	[h,w,d] = size(A);
	cx = w/2;
	cy = h/2;
	cz = d/2;
	
	theta = linspace(0, 2*pi, d+1);
	theta(end) = [];
	
	rho = logspace(0, log10(min([w-cx cx-1 d-cz cz-1])), w)';
	
	xx = zeros(h,w,d);
	yy = zeros(h,w,d);
	zz = zeros(h,w,d);
	for i = 1:h
		xx(i,:,:) = rho*sin(theta) + cx;
		yy(i,:,:) = i;
		zz(i,:,:) = rho*cos(theta) + cz;
	end
	
	B = interp3(A, xx, yy, zz, 'linear');
	
	mask = (xx>h) | (xx<1) | (zz>d) | (zz<1);
	B(mask) = 0;

end

