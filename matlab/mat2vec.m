function [ vec ] = mat2vec( mat )
	[m,n] = size(mat);
	vec = zeros(1, m*n);
	for i = 1:m
		for j = 1:n
			index = j + (i-1)*n;
			vec(index) = mat(i,j);
		end
	end
end

