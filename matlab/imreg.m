directory = '/home/martin/Documents/R02_NidarosGoodLarge02_dicom/';
sample = 10;
volume = loadDicomVolume(directory, sample);
fprintf('Volume loaded\n');

rot1 = [cosd(30) -sind(30) 0 0; sind(30) cosd(30) 0 0; 0 0 1 0; 0 0 0 1];
rot2 = [1 0 0 0; 0 cosd(30) -sind(30) 0; 0 sind(30) cosd(30) 0; 0 0 0 1];
tform = affine3d(rot2*rot1);
rotVolume = imwarp(volume, tform);
fprintf('Rotated volume created\n');

[optimizer,metric] = imregconfig('monomodal');
rVolume = imref3d(size(volume));
rRotVolume = imref3d(size(rotVolume));
estimate = imregtform(rotVolume, rRotVolume, volume, rVolume, 'rigid', optimizer, metric);
fprintf('Transformation estimated\n');