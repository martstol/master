function [ theta ] = estimate_rotation( I, J, axis )

	assert(strcmp(axis, 'z') || strcmp(axis, 'y') || strcmp(axis, 'x'), ...
		'Invalid axis. Valid values are ''x'', ''y'' or ''z''.');

	[h,w,d] = size(I);
	FI = fftshift(fftn(I));
	FJ = fftshift(fftn(J));
	HPF = make_highpass_filter(h,w,d);
	
	% Convolve the magnitude of the FFT with a high pass filter
	% and transform the high passed FFT phase to Log Polar space
	if (strcmp(axis, 'z'))
		LI = log_polar_trans_z(HPF .* abs(FI));
		LJ = log_polar_trans_z(HPF .* abs(FJ));
	elseif (strcmp(axis, 'y'))
		LI = log_polar_trans_y(HPF .* abs(FI));
		LJ = log_polar_trans_y(HPF .* abs(FJ));
	elseif (strcmp(axis, 'x'))
		LI = log_polar_trans_x(HPF .* abs(FI));
		LJ = log_polar_trans_x(HPF .* abs(FJ));
	end

	[i,j,k,~] = phase_correlation(LI, LJ);
	
	if (i > (h / 2))
		i = i - h;
	end
	if (j > (w / 2))
		j = j - w;
	end
	if (k > (d / 2))
		k = k - d;
	end
	
	% Compute angle of rotation
	if (strcmp(axis, 'z'))
		theta = 360 * j / w;
	elseif (strcmp(axis, 'y'))
		theta = 360 * k / d;
	elseif (strcmp(axis, 'x'))
		theta = 360 * k / d;
	end
	
end

