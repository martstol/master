function [i,j,k,value] = find_max( A )

value = max(max(max(A)));
[i,j,k] = ind2sub(size(A), (find(A == value)));

end

