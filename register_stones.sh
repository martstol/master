#!/bin/bash

function vdr {
	./vdr "$1" "$2"
}


drive="/media/martin/My_Passport/"
round1="StoneCollection_XCT_Round01_20140820/"
round2="StoneCollection_XCT_Round02_20150219/"

stone1=$drive$round1"20140918_XCT_Elefsis_Large_01/OTB20140918_ElefsisLarge1_01/ElefsisL1-dicom/"
stone2=$drive$round2"20150218_XCT_Elefsis_Large_01/OTB20150218_ElefsisLarge1_01/ElefsisL1_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140826_XCT_Elefsis_Large_02/OTB20140826_ElefsisLarge2_01/ElefsisL2-dicom/"
stone2=$drive$round2"20150218_XCT_Elefsis_Large_02/OTB20150218_ElefsisLarge2_01/ElefsisL2_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140826_XCT_Elefsis_Small_01/OTB20140826_ElefsisSmall1_01/ElefsisS1-dicom/"
stone2=$drive$round2"20150218_XCT_Elefsis_Small_01/OTB20150218_ElefsisSmall1/ElefsisS1_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140826_XCT_Nidaros_Bad_Large_01/OTB20140826_NidarosBadLarge1_01/NidarosBL1-dicom/"
stone2=$drive$round2"20150216_XCT_Nidaros_Bad_Large_01/OTB20150216_NidarosBadLarge1_01/NidarosBL1_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140826_XCT_Nidaros_Good_Large_01/OTB20140826_NidarosGoodLarge1_01/NidarosGL1-dicom/"
stone2=$drive$round2"20150216_XCT_Nidaros_Good_Large_01/OTB20150216_NidarosGoodLarge1_01/NidarosGL1_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140826_XCT_Nidaros_Good_Large_02/OTB20140826_NidarosGoodLarge2_01/NidarosGL2-dicom/"
stone2=$drive$round2"20150216_XCT_Nidaros_Good_Large_02/OTB20150216_NidarosGoodLarge2_01/NidarosGL2_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140822_XCT_Nidaros_Good_Small_01/OTB20140822_NidarosGoodSmall1-2_02/dicom-1hour/"
stone2=$drive$round2"20150218_XCT_Nidaros_Good_Small_01/OTB20150218_NidarosGoodSmall1_01/NidarosGS1_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140901_XCT_Nidaros_Bad_Large_03/OTB20140901_NidarosBadLarge3_01/NidarosBL3-dicom/"
stone2=$drive$round2"20150218_XCT_Nidaros_Bad_Large_03/OTB20150218_NidarosBadLarge3_01/NidarosBL3_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140901_XCT_Nidaros_Good_Large_03/OTB20140901_NidarosGoodLarge3_01/NidarosGL3-dicom/"
stone2=$drive$round2"20150216_XCT_Nidaros_Good_Large_03/OTB20150216_NidarosGoodLarge3_01/NidarosGL3_dicom/"
vdr $stone1 $stone2

stone1=$drive$round1"20140918_XCT_Nidaros_Bad_Small_01/OTB20140918_NidarosBadSmall1_01/NidarosBS1-dicom/"
stone2=$drive$round2"20150218_XCT_Nidaros_Bad_Small_01/OTB20150218_NidarosBadSmall1_01/NidarosBS1_dicom/"
vdr $stone1 $stone2
