#include "fftw_complex_math.h"

#include <cmath>

namespace vdr {
	
	double abs(fftw_complex const& c) {
		return std::sqrt(c[0]*c[0] + c[1]*c[1]);
	}
	
}