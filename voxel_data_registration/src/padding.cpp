#include "padding.h"

#include <iostream>

namespace vdr {
	
	array3<fftw_complex> pad(array3<fftw_complex> const& in, size_t dx, size_t dy, size_t dz) {
		size_t w = in.getWidth(), h = in.getHeight(), d = in.getDepth();
		
		array3<fftw_complex> out{w+dx, h+dy, d+dz};
		
		#pragma omp parallel for
		for(size_t i = 0; i < out.size(); i++) {
			out[i][0] = 0;
			out[i][1] = 0;
		}
		
		#pragma omp parallel for
		for (size_t k = 0; k < d; k++) {
			for (size_t j = 0; j < h; j++) {
				for (size_t i = 0; i < w; i++) {
					out(i, j, k)[0] = in(i, j, k)[0];
					out(i, j, k)[1] = in(i, j, k)[1];
				}
			}
		}
		
		return out;
		
	}
	
	void padVolumes(array3<fftw_complex> & a, array3<fftw_complex> & b) {
		int dx = a.getWidth() - b.getWidth();
		int dy = a.getHeight() - b.getHeight();
		int dz = a.getDepth() - b.getDepth();
		
		size_t xPad = dx > 0 ? dx : 0;
		size_t yPad = dy > 0 ? dy : 0;
		size_t zPad = dz > 0 ? dz : 0;
		if ( xPad || yPad || zPad ) {
			b = pad(b, xPad, yPad, zPad);
		}
		
		xPad = dx < 0 ? -dx : 0;
		yPad = dy < 0 ? -dy : 0;
		zPad = dz < 0 ? -dz : 0;
		if ( xPad || yPad || zPad ) {
			a = pad(a, xPad, yPad, zPad);
		}
	}
	
}