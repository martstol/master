#include "crosscorr.h"
#include "oocvolume.h"
#include "showvolume.h"
#include "optimization.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include <iostream>
#include <cassert>
#include <cmath>
#include <vector>
#include <functional>
#include <cassert>

namespace vdr { namespace crosscorr {
	
	
	double crossCorrelation(ooc::Volume const& fixed, ooc::Volume const& moving, glm::mat4 mat,
							size_t sx, size_t sy, size_t sz, size_t ex, size_t ey, size_t ez) {
		assert(sx < ex);
		assert(sy < ey);
		assert(sz < ez);
		assert(ex <= fixed.getWidth());
		assert(ey <= fixed.getHeight());
		assert(ez <= fixed.getDepth());
		
		double n0 = 0;
		double n1 = 0;
		double n2 = 0;
		
		for (size_t z = sz; z < ez; z++) {
			for (size_t y = sy; y < ey; y++) {
				for (size_t x = sx; x < ex; x++) {
					double v0 = fixed(x,y,z);
					double v1 = moving(mat*glm::vec4(x,y,z,1));
					n0 += v0*v1;
					n1 += v0*v0;
					n2 += v1*v1;
				}
			}
		}
		
		return n0 / std::sqrt(n1*n2);
	}
	
	glm::mat4 createTransform(Vector x, glm::vec3 center) {
		assert(x.size() == 6);
		
		auto c0 = glm::translate(glm::mat4(), -center);
		auto c1 = glm::translate(glm::mat4(), center);
		auto T = glm::translate(glm::mat4(), glm::vec3(x[0], x[1], x[2]));
		auto Rx = glm::rotate(glm::mat4(), glm::radians(x[3]), AXIS_VECTOR[X_AXIS]);
		auto Ry = glm::rotate(glm::mat4(), glm::radians(x[4]), AXIS_VECTOR[Y_AXIS]);
		auto Rz = glm::rotate(glm::mat4(), glm::radians(x[5]), AXIS_VECTOR[Z_AXIS]);
		
		return T*c1*Rx*Ry*Rz*c0;
	}
	
	Function createCompareFunction(ooc::Volume const& fixed, ooc::Volume const& moving, float cutFraction) {
		glm::vec3 center = 0.5f * fixed.size();
		glm::vec3 s = center - cutFraction*center;
		glm::vec3 e = center + cutFraction*center;
		return [s,e,&fixed,&moving] (Vector x) -> real_t {
			glm::mat4 mat = createTransform(x, 0.5f*fixed.size());
			return crossCorrelation(fixed, moving, mat, s.x, s.y, s.z, e.x, e.y, e.z);
		};
	}
	
	
	glm::mat4 registerTransformation(ooc::Volume const& fixed, ooc::Volume const& moving, float cutFraction, size_t maxIterations) {
		assert(cutFraction <= 1.0f);
		
		auto cc = createCompareFunction(fixed, moving, cutFraction);
		Vector x({0, 0, 0, 0, 0, 0});
		Vector h({4.0f, 4.0f, 4.0f, 1.0f, 1.0f, 1.0f});
		x = hillClimbing(cc, x, h, maxIterations, 0.01);

		auto mat = createTransform(x, fixed.size());
		return glm::inverse(mat);
	}
	
	
}}
