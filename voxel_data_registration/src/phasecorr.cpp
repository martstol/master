#include "phasecorr.h"
#include "max.h"
#include "fft.h"
#include "fftw_complex_math.h"
#include "filter.h"
#include "cylindrical.h"
#include "transform.h"
#include "padding.h"
#include "showvolume.h"

#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <cassert>
#include <cmath>

namespace vdr { namespace phasecorr {
	
	void crossPowerSpectrum(array3<fftw_complex> & fixed,
							array3<fftw_complex> & moving) {
		assert(fixed.getWidth() == moving.getWidth());
		assert(fixed.getHeight() == moving.getHeight());
		assert(fixed.getDepth() == moving.getDepth());
		
		#pragma omp parallel for
		for(size_t i = 0; i < fixed.size(); i++) {
			fftw_complex c = {0};
			fftw_complex const& a = fixed[i];
			fftw_complex const& b = moving[i];
			
			c[0] = a[0]*b[0] + a[1]*b[1];
			c[1] = a[0]*b[1] - a[1]*b[0];
			
			double t = abs(c);
			if (t > 0) {
				fixed[i][0] = c[0] / t;
				fixed[i][1] = c[1] / t;
			} else {
				fixed[i][0] = 0;
				fixed[i][1] = 0;
			}
			
		}
	}
	
	void phaseCorrelation(array3<fftw_complex> & fixed, 
						  array3<fftw_complex> & moving) {		
		fft(fixed);
		fft(moving);
		
		crossPowerSpectrum(fixed, moving);
		
		ifft(fixed);
	}
	
	double compare(array3<fftw_complex> & fixed, 
				   array3<fftw_complex> & moving) {		
		phaseCorrelation(fixed, moving);
		
		std::array<size_t, 3> peak = find_max(fixed);
		
		return fixed(peak[0], peak[1], peak[2])[0];
	}
	
	double registerRotation(array3<fftw_complex> const& fixed,
							array3<fftw_complex> const& moving,
							Axis axis) {
		auto F = fixed.copy();
		auto M = moving.copy();
		
		fft(F);
		fft(M);
		
		F = fftshift(F);
		M = fftshift(M);
		
		applyFilter(F, highPassFilter);
		applyFilter(M, highPassFilter);
		
		F = toCylindricalCoordinates(F, axis);
		M = toCylindricalCoordinates(M, axis);
		
		phaseCorrelation(F, M);
		
		std::array<size_t, 3> pos = find_max(F);
		
		return (360.0 * pos[1]) / F.getHeight();
	}
	
	glm::vec3 registerTranslation(array3<fftw_complex> const& fixed,
							 array3<fftw_complex> const& moving) {
		auto F = fixed.copy();
		auto M = moving.copy();
		
		phaseCorrelation(F, M);
		
		std::array<size_t, 3> peak = find_max(F);
		glm::vec3 v{peak[0], peak[1], peak[2]};
		
		if (v.x > F.getWidth() / 2) {
			v.x -= F.getWidth();
		}
		if (v.y > F.getHeight() / 2) {
			v.y -= F.getHeight();
		}
		if (v.z > F.getDepth() / 2) {
			v.z -= F.getDepth();
		}
		
		return v;
	}
	
	double findBestAngle(array3<fftw_complex> const& fixed, array3<fftw_complex> const& moving, double angle, Axis axis) {
		double bestAngle = 0;
		double bestCmp = 0;
		for (double delta : {0.0, 180.0}) {
			auto f0 = fixed.copy();
			auto f1 = transform(moving, glm::rotate(glm::mat4(1.0), -degToRad(float(angle+delta)), AXIS_VECTOR[axis]));
			padVolumes(f0, f1);
			double cmp = compare(f0, f1);
			if (cmp > bestCmp) {
				bestAngle = angle+delta;
				bestCmp = cmp;
			}
		}
		while(bestAngle >= 360) {bestAngle -= 360;}
		return bestAngle;
	}
	
	glm::mat4 registerTransformation(ooc::Volume const& oocFixed, ooc::Volume const& oocMoving,
									 size_t sampleFrequency, size_t maxIterations) {
		array3<fftw_complex> fixed = ooc::createDownsampledVolume(oocFixed, sampleFrequency);
		array3<fftw_complex> moving = ooc::createDownsampledVolume(oocMoving, sampleFrequency);

		auto f0 = fixed.copy();
		auto f1 = moving.copy();
		glm::mat4 mat(1.0f);
		size_t iterations = 0;
		size_t numZeroEstimates = 0;
		do {
			Axis axis = INDEXED_AXISES[iterations%3];
			padVolumes(f0, f1);
			double angle = phasecorr::registerRotation(f0, f1, axis);
			if(axis == Y_AXIS) {angle = -angle + 360;}
			angle = findBestAngle(f0, f1, angle, axis);

			if (std::abs(angle) < 1 || std::abs(angle) > 359) {
				numZeroEstimates++;
			} else {
				numZeroEstimates = 0;
			}
			
			glm::mat4 rot = glm::rotate(glm::mat4(1.0f), -degToRad(float(angle)), AXIS_VECTOR[axis]);
			mat = mat*rot;
			f1 = transform(moving, mat);
			f0 = fixed.copy();
		} while(iterations++ < maxIterations && numZeroEstimates < 3);
		
		mat = [&]() {
			glm::vec3 v, _;
			std::tie(v, _) = findTransformedAABB(getVolumeCorners(moving), mat);
			glm::mat4 trans = glm::translate(glm::mat4(1.0f), -float(sampleFrequency)*v);
			return trans*mat;
		}();
		
		padVolumes(f0, f1);
		glm::vec3 v(0.0f);
		do {
			v = phasecorr::registerTranslation(f0, f1);
			f1 = translate(f1, -v.x, -v.y, -v.z);
			mat = glm::translate(glm::mat4(1.0f), -float(sampleFrequency)*v)*mat;
		} while(glm::length(v) > 0.01);

		return mat;
	}
	
}}
