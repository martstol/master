#include "transform.h"
#include "interpolate.h"

namespace vdr {
	
	array3<fftw_complex> translate(array3<fftw_complex> const& in, double dx, double dy, double dz) {
		array3<fftw_complex> out{in.getWidth(), in.getHeight(), in.getDepth()};
		
		#pragma omp parallel for
		for(size_t i = 0; i < out.size(); i++) {
			out[i][0] = 0;
			out[i][1] = 0;
		}
		
		for(size_t k = 0; k < out.getDepth(); k++) {
			for (size_t j = 0; j < out.getHeight(); j++) {
				for (size_t i = 0; i < out.getWidth(); i++) {
					double x = i-dx;
					double y = j-dy;
					double z = k-dz;
					if (0 <= x && x <= (in.getWidth()-1) &&
						0 <= y && y <= (in.getHeight()-1) &&
						0 <= z && z <= (in.getDepth()-1)) {
						interpolate::trilinear(x, y, z, in, out(i, j, k));
					}
				}
			}
		}
		
		return out;
	}
	
}