#include "vdr.h"
#include "crosscorr.h"

#include <fftw3.h>
#include <omp.h>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <chrono>
#include <string>

int main(int argc, char * argv[]) {
	if (argc < 3) {
		std::cerr << "Usage: " << argv[0] << " in_dir1 in_dir2 (optional)out_dir" << std::endl;
		return 1;
	}
	
	if (!fftw_init_threads()) {
		std::cerr << "There was an error initializing fftw with threads." << std::endl;
		return 1;
	}
	
	std::cout << "Available OpenMP Threads: " << omp_get_max_threads() << std::endl;
	fftw_plan_with_nthreads(omp_get_max_threads());
	
	auto start = std::chrono::steady_clock::now();
	if (argc > 3) {
		vdr::voxelDataRegistration(argv[1], argv[2], 1, 50, 1, 50, true, argv[3]);
	} else {
		vdr::voxelDataRegistration(argv[1], argv[2], 1, 50, 1, 50);
	}
	auto end = std::chrono::steady_clock::now();
	
	auto totalTime = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
	auto seconds = totalTime % 60;
	auto minutes = (totalTime / 60) % 60;
	auto hours = totalTime / (60*60);
	std::cout << "Time: " << hours << "h " << minutes << "m " << seconds << "s" << std::endl;
	
	return 0;
}
