#include "optimization.h"

#include <boost/numeric/ublas/lu.hpp>

#include <functional>
#include <cassert>
#include <exception>

namespace vdr {

	using size_type = Vector::size_type;
	using boost::numeric::ublas::prod;
	using boost::numeric::ublas::norm_inf;

	Matrix hessian(Function func, Vector x, Vector h) {
		Matrix H(x.size(), x.size());

		for (size_type i = 0; i < x.size(); i++) {
			for (size_type j = 0; j < x.size(); j++) {
				Vector v0 = x;
				Vector v1 = x;
				Vector v2 = x;
				Vector v3 = x;
				v0[i] += h[i];
				v0[j] += h[j];
				v1[i] += h[i];
				v1[j] -= h[j];
				v2[i] -= h[i];
				v2[j] += h[j];
				v3[i] -= h[i];
				v3[j] -= h[j];
				H(i, j) = (func(v0) + func(v1) + func(v2) + func(v3)) / (4*h[i]*h[j]);
			}
		}

		return H;
	}

	Matrix inverse(Matrix A) {
		using namespace boost::numeric::ublas;
		permutation_matrix<std::size_t> pm(A.size1());

		int res = lu_factorize(A, pm);
		if (res != 0) {
			std::cout << "Cannot lu-factorize matrix: " << std::endl;
			std::cout << A << std::endl;
			throw std::runtime_error("Cannot lu-factorize matrix");
		}

		Matrix I = identity_matrix<real_t>(A.size1());

		lu_substitute(A, pm, I);

		return I;
	}

	Matrix hessianInv(Function func, Vector x, Vector h) {
		Matrix H = hessian(func, x, h);
		return inverse(H);
	}

	Vector newtonsMethod(Function func, Vector x, Vector h,
						 size_t maxIterations, real_t step, real_t tol) {
		assert(x.size() == h.size());

		for (size_t itr = 0; itr < maxIterations; itr++) {
			Vector dx = step * prod(hessianInv(func, x, h), gradient(func, x, h));
			x = x - dx;
			if (norm_inf(dx) < tol) {break;}
		}

		return x;
	}


}
