#include "vdr.h"
#include "phasecorr.h"
#include "transform.h"
#include "showvolume.h"
#include "crosscorr.h"
#include "oocvolume.h"
#include "array.h"
#include "noise.h"
#include "padding.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <iostream>
#include <vector>

namespace vdr {

	
	void voxelDataRegistration(std::string const& filepath1, std::string const& filepath2,
							   size_t downscaleFilterSize, size_t phaseOneMaxItr,
							   double subvolumeCut, size_t phaseTwoMaxItr,
							   bool output, std::string const& outputPath) {
		ooc::Volume fixed(filepath1);
		ooc::Volume moving(filepath2);

		glm::mat4 m0 = phasecorr::registerTransformation(fixed, moving, downscaleFilterSize, phaseOneMaxItr);
		moving = transform(moving, m0);

		glm::mat4 m1 = crosscorr::registerTransformation(fixed, moving, subvolumeCut, phaseTwoMaxItr);
		moving = transform(moving, m1);
		
		glm::mat4 mat = m1*m0;
		std::cout << "Registered transformation:" << std::endl;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				std::cout << mat[i][j] << "  ";
			}
			std::cout << std::endl;
		}
		
		if (output) {
			ooc::outputVolume(moving, outputPath);
		}
	}
	
	
}
