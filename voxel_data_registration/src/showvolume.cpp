#include "showvolume.h"
#include "transform.h"

#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cassert>
#include <sstream>
#include <iostream>
#include <stdexcept>

/*
 * 
 * WARNING: HERE BE DRAGONS!
 * (Luckily no vital code should be here)
 * 
 */

namespace vdr {

	cv::Mat volumeSliceToImage(vdr::array3<fftw_complex> const& volume, size_t slice, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);
		
		if (axis == Z_AXIS) {
			assert(slice < volume.getDepth());
			cv::Mat img{volume.getHeight(), volume.getWidth(), CV_8UC1};
			for (size_t j = 0; j < volume.getHeight(); j++) {
				for (size_t i = 0; i < volume.getWidth(); i++) {
					img.at<uint8_t>(j, i) = (uint8_t) volume(i,j,slice)[0];
				}
			}
			return img;
		} else if (axis == Y_AXIS) {
			assert(slice < volume.getHeight());
			cv::Mat img{volume.getDepth(), volume.getWidth(), CV_8UC1};
			for (size_t j = 0; j < volume.getDepth(); j++) {
				for (size_t i = 0; i < volume.getWidth(); i++) {
					img.at<uint8_t>(j, i) = (uint8_t) volume(i,slice,j)[0];
				}
			}
			return img;
		} else if (axis == X_AXIS) {
			assert(slice < volume.getWidth());
			cv::Mat img{volume.getDepth(), volume.getHeight(), CV_8UC1};
			for (size_t j = 0; j < volume.getDepth(); j++) {
				for (size_t i = 0; i < volume.getHeight(); i++) {
					img.at<uint8_t>(j, i) = (uint8_t) volume(slice,i,j)[0];
				}
			}
			return img;
		} else {
			throw std::runtime_error{"Invalid axis"};
		}
	}
	
	cv::Mat volumeSliceToImage(ooc::Volume const& volume, size_t slice, glm::mat4 mat, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);
		
		mat = glm::inverse(mat);
		
		if (axis == Z_AXIS) {
			assert(slice < volume.getDepth());
			cv::Mat img{volume.getHeight(), volume.getWidth(), CV_8UC1};
			for (size_t j = 0; j < volume.getHeight(); j++) {
				for (size_t i = 0; i < volume.getWidth(); i++) {
					glm::vec4 v = glm::vec4(i, j, slice, 1.0f);
					img.at<uint8_t>(j, i) = (uint8_t) volume(mat*v);
				}
			}
			return img;
		} else if (axis == Y_AXIS) {
			assert(slice < volume.getHeight());
			cv::Mat img{volume.getDepth(), volume.getWidth(), CV_8UC1};
			for (size_t j = 0; j < volume.getDepth(); j++) {
				for (size_t i = 0; i < volume.getWidth(); i++) {
					glm::vec4 v = glm::vec4(i, slice, j, 1.0f);
					img.at<uint8_t>(j, i) = (uint8_t) volume(mat*v);
				}
			}
			return img;
		} else if (axis == X_AXIS) {
			assert(slice < volume.getWidth());
			cv::Mat img{volume.getDepth(), volume.getHeight(), CV_8UC1};
			for (size_t j = 0; j < volume.getDepth(); j++) {
				for (size_t i = 0; i < volume.getHeight(); i++) {
					glm::vec4 v = glm::vec4(slice, i, j, 1.0f);
					img.at<uint8_t>(j, i) = (uint8_t) volume(mat*v);
				}
			}
			return img;
		} else {
			throw std::runtime_error{"Invalid axis"};
		}
	}
	
	void storeVolumeSlice(array3<fftw_complex> const& volume, size_t slice, Axis axis, std::string const& path) {
		if (!boost::filesystem::exists(path)) {
			throw std::runtime_error{path + " does not exist"};
		}
		if (!boost::filesystem::is_directory(path)) {
			throw std::runtime_error{path + " is not a directory"};
		}
		
		cv::Mat img = volumeSliceToImage(volume, slice, axis);
		std::stringstream ss;
		ss << path << slice << ".png";
		assert(cv::imwrite(ss.str(), img));
	}
	
	void outputVolumeSlices(array3<fftw_complex> const& volume, std::string const& path, size_t frequency) {
		
		for(size_t slice = 0; slice < volume.getDepth(); slice += frequency) {
			std::stringstream ss;
			ss << path << "z_axis/";
			storeVolumeSlice(volume, slice, Z_AXIS, ss.str());
		}
		for(size_t slice = 0; slice < volume.getHeight(); slice += frequency) {
			std::stringstream ss;
			ss << path << "y_axis/";
			storeVolumeSlice(volume, slice, Y_AXIS, ss.str());
		}
		for(size_t slice = 0; slice < volume.getWidth(); slice += frequency) {
			std::stringstream ss;
			ss << path << "x_axis/";
			storeVolumeSlice(volume, slice, X_AXIS, ss.str());
		}
		
	}

	void showVolumeSlice(array3<fftw_complex> const& volume, size_t slice, Axis axis) {
		cv::Mat img = volumeSliceToImage(volume, slice, axis);

		cv::imshow("Volume", img);
		cv::waitKey(0);
	}
	
	void outputImage(std::string path, cv::Mat const & img) {
		static int i = 0;
		
		std::stringstream ss;
		ss << path << (i++) << ".png";
		
		std::cout << "Writing image: " << ss.str() << std::endl;
		assert(cv::imwrite(ss.str(), img));
	}
	
	void showVolumeSlice(ooc::Volume const& volume, size_t slice, glm::mat4 mat, Axis axis, bool output) {
		cv::Mat img = volumeSliceToImage(volume, slice, mat, axis);
		if (output) {outputImage("", img);}
		cv::namedWindow("Volume", CV_WINDOW_NORMAL | CV_WINDOW_FREERATIO | CV_GUI_EXPANDED);
		cv::imshow("Volume", img);
		cv::waitKey(0);
	}
	
	void showVolumeSliceDiff(ooc::Volume const& fixed, ooc::Volume const& moving, size_t slice0, size_t slice1, glm::mat4 mat, Axis axis, bool output, std::string path) {
		cv::Mat img0 = volumeSliceToImage(fixed, slice0, glm::mat4(), axis);
		cv::Mat img1 = volumeSliceToImage(moving, slice1, mat, axis);
		
		cv::Mat img = cv::Mat::zeros(std::max(img0.rows, img1.rows), std::max(img0.cols, img1.cols), CV_8UC3);
		for (int j = 0; j < img.rows; j++) {
			for (int i = 0; i < img.cols; i++) {
				uint8_t f = 0;
				if (i < img0.cols && j < img0.rows) {
					f = img0.at<uint8_t>(j,i);
				}
				
				uint8_t m = 0;
				if (i < img1.cols && j < img1.rows) {
					m = img1.at<uint8_t>(j,i);
				}
				
				img.at<cv::Vec3b>(j, i) = cv::Vec3b(f, m, f);
			}
		}
		if (output) {outputImage(path, img);}
		else {
			cv::namedWindow("Volume", CV_WINDOW_NORMAL | CV_WINDOW_FREERATIO | CV_GUI_EXPANDED);
			cv::imshow("Volume", img);
			cv::waitKey(0);
		}
	}
	
}