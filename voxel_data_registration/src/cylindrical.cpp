#include "cylindrical.h"
#include "interpolate.h"

#include <glm/gtc/constants.hpp>

#include <cassert>
#include <cmath>
#include <stdexcept>

namespace vdr {
	
	void resample(array3<fftw_complex> const& in, fftw_complex & out,
				  double rho, double theta, double plane, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);

		double cx = in.getWidth()/2.0, cy = in.getHeight()/2.0, cz = in.getDepth()/2.0;
		double x = 0, y = 0, z = 0;
		
		if (axis == X_AXIS) {
			x = plane;
			y = rho*cos(theta) + (cy);
			z = rho*sin(theta) + (cz);
		} else if (axis == Y_AXIS) {
			x = rho*sin(theta) + (cx);
			y = plane;
			z = rho*cos(theta) + (cz);
		} else if (axis == Z_AXIS) {
			x = rho*cos(theta) + (cx);
			y = rho*sin(theta) + (cy);
			z = plane;
		} else {
			throw std::runtime_error("Invalid axis");
		}
		
		if (0 <= x && x <= (in.getWidth()-1) &&
			0 <= y && y <= (in.getHeight()-1) &&
			0 <= z && z <= (in.getDepth()-1)) {
			interpolate::trilinear(x, y, z, in, out);
		}
	}

	size_t getRadialDim(array3<fftw_complex> const& in, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);

		size_t w = in.getWidth(), h = in.getHeight(), d = in.getDepth();
		double cx = w/2.0, cy = h/2.0, cz=d/2.0;

		if (axis == X_AXIS) {
			return std::ceil(std::sqrt(cy*cy + cz*cz));
		} else if (axis == Y_AXIS) {
			return std::ceil(std::sqrt(cx*cx + cz*cz));
		} else if (axis == Z_AXIS) {
			return std::ceil(std::sqrt(cx*cx + cy*cy));
		} else {
			throw std::runtime_error("Invalid axis");
		}
	}

	size_t getAngularDim(array3<fftw_complex> const& in, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);
		
		size_t w = in.getWidth(), h = in.getHeight(), d = in.getDepth();
		double cx = w/2.0, cy = h/2.0, cz=d/2.0;

		double a = 360;
		if (axis == X_AXIS) {
			a = std::max(a, cy*cz / getRadialDim(in, axis));
		} else if (axis == Y_AXIS) {
			a = std::max(a, cx*cz / getRadialDim(in, axis));
		} else if (axis == Z_AXIS) {
			a = std::max(a, cx*cy / getRadialDim(in, axis));
		} else {
			throw std::runtime_error("Invalid axis");
		}
		return a;
	}

	size_t getHeightDim(array3<fftw_complex> const& in, Axis axis) {
		assert(axis == X_AXIS || axis == Y_AXIS || axis == Z_AXIS);
		
		if (axis == X_AXIS) {
			return in.getWidth();
		} else if (axis == Y_AXIS) {
			return in.getHeight();
		} else if (axis == Z_AXIS) {
			return in.getDepth();
		} else {
			throw std::runtime_error("Invalid axis");
		}
	}
	
	array3<fftw_complex> allocateCylindricalCoordOutput(array3<fftw_complex> const& in, Axis axis) {
		size_t radial = getRadialDim(in, axis);
		size_t angular = getAngularDim(in, axis);
		size_t height = getHeightDim(in, axis);
		array3<fftw_complex> out(radial, angular, height);

		#pragma omp parallel for
		for (size_t i = 0; i < out.size(); i++) {
			out[i][0] = 0;
			out[i][1] = 0;
		}

		return out;
	}

	array3<fftw_complex> toCylindricalCoordinates(array3<fftw_complex> const& in, Axis axis) {
		array3<fftw_complex> out = allocateCylindricalCoordOutput(in, axis);
		
		#pragma omp parallel for
		for (size_t k = 0; k < out.getDepth(); k++) {
			for (size_t j = 0; j < out.getHeight(); j++) {
				for (size_t i = 0; i < out.getWidth(); i++) {
					double rho = i;
					double theta = (2*glm::pi<double>()*j)/out.getHeight();
					double plane = k;
					resample(in, out(i, j, k), rho, theta, plane, axis);
				}
			}
		}
		
		return out;
	}
	
}
