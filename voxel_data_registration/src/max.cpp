#include "max.h"

#include <cassert>

namespace vdr {
	
	std::array<size_t, 3> find_max(array3<fftw_complex> const& array) {
		assert(array.size() > 0);
		
		std::array<size_t, 3> pos = {0, 0, 0};
		
		for (size_t k = 0; k < array.getDepth(); k++) {
			for (size_t j = 0; j < array.getHeight(); j++) {
				for (size_t i = 0; i < array.getWidth(); i++) {
					if (array(i, j, k)[0] > array(pos[0], pos[1], pos[2])[0]) {
						pos[0] = i;
						pos[1] = j;
						pos[2] = k;
					}
				}
			}
		}
		
		return pos;
	}
	
}