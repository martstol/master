#include "volumefiles.h"

#include <boost/math/constants/constants.hpp>

#include <string>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <cmath>

namespace vdr {

	VolumeFiles::VolumeFiles(boost::filesystem::path const& directoryPath) : directory(directoryPath) {
		using namespace boost;
		
		if (!filesystem::exists(directoryPath)) {
			throw std::runtime_error{directoryPath.string() + " does not exist"};
		}
		
		if (!filesystem::is_directory(directoryPath)) {
			throw std::runtime_error{directoryPath.string() + " is not a directory"};
		}
		
		filesystem::recursive_directory_iterator begin{directoryPath};
		filesystem::recursive_directory_iterator end{};
		std::back_insert_iterator<decltype(fileList)> inserter{fileList};
		std::copy_if(begin, end, inserter, 
			[] (filesystem::directory_entry const& entry) {
				return filesystem::is_regular_file(entry) && entry.path().extension() == DCM_FILE_EXTENTION;
			});
		
		std::sort(fileList.begin(), fileList.end());
	}
	
	VolumeSlice VolumeFiles::loadVolumeSlice(size_t slice) const {
		return VolumeSlice(fileList[slice]);
	}

	size_t VolumeFiles::numFiles() const {
		return fileList.size();
	}

}