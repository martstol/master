#include "volumeslice.h"

#include <cassert>
#include <exception>

namespace vdr {

	VolumeSlice::VolumeSlice(boost::filesystem::path const& filePath) : path(filePath), 
		dicomImage{new DicomImage{filePath.string().c_str()}} {
		
		if (dicomImage->getStatus() != EIS_Normal) {
			throw std::runtime_error{"Cannot load dicom image " + filePath.string()};
		}
		
		// Assert that the image has the expected format
		assert(dicomImage->getInterData()->getRepresentation() == EPR_Uint16);
		assert(dicomImage->getDepth() == 16);
		assert(dicomImage->isMonochrome());
	}

	size_t vdr::VolumeSlice::numColumns() const {
		return dicomImage->getWidth();
	}
	
	
	size_t vdr::VolumeSlice::numRows() const {
		return dicomImage->getHeight();
	}
	
	
	VolumeSlice::value_type VolumeSlice::operator()(size_t row, size_t column) const {
		DiPixel const * interData = dicomImage->getInterData();
		value_type const * data = static_cast<value_type const *>(interData->getData());
		return data[column + row * dicomImage->getWidth()];
	}
	
	
	VolumeSlice::value_type VolumeSlice::at(size_t row, size_t column) const {
		assert(row < numRows());
		assert(column < numColumns());
		return operator()(row, column);
	}
	
	
	VolumeSlice::value_type vdr::VolumeSlice::operator[](size_t i) const {
		DiPixel const * interData = dicomImage->getInterData();
		value_type const * data = static_cast<value_type const *>(interData->getData());
		return data[i];
	}

}