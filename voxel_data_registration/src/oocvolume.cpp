#include "oocvolume.h"
#include "volumefiles.h"
#include "interpolate.h"

#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>

#include <cmath>
#include <limits>
#include <sstream>

namespace vdr { namespace ooc {
	
	Volume::Volume(std::string const& filepath) : vector(std::make_shared<Vector>()) {
		std::cout << "Loading volume " << filepath << " ..." << std::endl;
		VolumeFiles files(filepath);
		
		assert(files.numFiles() > 0);
		VolumeSlice slice = files.loadVolumeSlice(0);
		width = slice.numColumns();
		height = slice.numRows();
		depth = files.numFiles();
		vector->resize(width*height*depth);
		
		double rescale = double(std::numeric_limits<ooc::value_type>::max()) / double(std::numeric_limits<VolumeSlice::value_type>::max());
		
		for (size_t k = 0; k < depth; k++) {
			slice = files.loadVolumeSlice(k);
			for (size_t j = 0; j < height; j++) {
				for (size_t i = 0; i < width; i++) {
					operator()(i,j,k) = static_cast<value_type>(rescale*slice.at(j, i));
				}
			}
		}
		std::cout << "Volume loaded" << std::endl;
	}
	
	Volume::Volume(size_t w, size_t h, size_t d) : width(w), height(h), depth(d), vector(std::make_shared<Vector>()) {
		vector->resize(w*h*d);
	}
	
	size_t Volume::index(size_t i, size_t j, size_t k) const {
		return i + j*width + k*width*height;
	}
	
	value_type & Volume::operator()(size_t i, size_t j, size_t k) {
		assert(i < width && j < height && k < depth);
		return (*vector)[index(i, j, k)];
	}
	
	value_type const& Volume::operator()(size_t i, size_t j, size_t k) const {
		assert(i < width && j < height && k < depth);
		return (*vector)[index(i, j, k)];
	}
	
	value_type Volume::operator()(glm::vec4 v) const {
		return operator()(glm::vec3(v));
	}
	
	value_type Volume::operator()(glm::vec3 v) const {
		if (0 <= v.x && v.x <= (width-1) &&
			0 <= v.y && v.y <= (height-1) &&
			0 <= v.z && v.z <= (depth-1)) {
			return interpolate::trilinear(v.x, v.y, v.z, *this);
		} else {
			return 0;
		}
	}
	
	
	
	array3<double> createGaussianFilter(size_t size, double sigma = 0.25) {
		assert(size % 2 == 1);
		
		double center = (size-1.0)/2.0;
		auto gaussian3d = [sigma](double x, double y, double z) -> double {
			return std::exp(-(x*x+y*y+z*z)/(2*sigma*sigma));
		};
		auto nGaussian3d = [gaussian3d,center](double x, double y, double z) -> double {
			return gaussian3d(x/center, y/center, z/center);
		};
		auto cnGaussian3d = [nGaussian3d,center](double x, double y, double z) -> double {
			return nGaussian3d(x-center, y-center, z-center);
		};
		
		double sum = 0;
		array3<double> filter(size, size, size);
		for (size_t i = 0; i < size; i++) {
			for (size_t j = 0; j < size; j++) {
				for (size_t k = 0; k < size; k++) {
					filter(i, j, k) = cnGaussian3d(i, j, k);
					sum += filter(i, j, k);
				}
			}
		}
		
		for (size_t i = 0; i < filter.size(); i++) {
			filter[i] /= sum;
		}
		
		return filter;
	}
	
	array3<double> createAveragingFilter(size_t size) {
		array3<double> filter(size, size, size);
		for (size_t i = 0; i < filter.size(); i++) {
			filter[i] = 1.0 / (size*size*size);
		}
		return filter;
	}
	
	double applyFilter(ooc::Volume const& volume, size_t i0, size_t j0, size_t k0, size_t sampleFreqency, array3<double> const& filter) {
		
		double sum = 0.0;
		for (size_t k = 0; k < sampleFreqency; k++) {
			for (size_t j = 0; j < sampleFreqency; j++) {
				for (size_t i = 0; i < sampleFreqency; i++) {
					size_t x = std::min(i0 * sampleFreqency + i, volume.getWidth()-1);
					size_t y = std::min(j0 * sampleFreqency + j, volume.getHeight()-1);
					size_t z = std::min(k0 * sampleFreqency + k, volume.getDepth()-1);
					sum += volume(x, y, z) * filter(i, j, k);
				}
			}
		}
		
		return sum;
	}

	array3<fftw_complex> createDownsampledVolume(ooc::Volume const& in, size_t sampleFreqency) {
		assert(sampleFreqency > 0);
		array3<fftw_complex> out(in.getWidth()/sampleFreqency, 
								 in.getHeight()/sampleFreqency,
								 in.getDepth()/sampleFreqency);
		if (sampleFreqency == 1) {
			for (size_t k = 0; k < out.getDepth(); k++) {
				for (size_t j = 0; j < out.getHeight(); j++) {
					for (size_t i = 0; i < out.getWidth(); i++) {
						out(i, j, k)[0] = in(i, j, k);
						out(i, j, k)[1] = 0;
					}
				}	
			}
		} else {
			auto filter = createGaussianFilter(sampleFreqency);
			
			for (size_t k = 0; k < out.getDepth(); k++) {
				for (size_t j = 0; j < out.getHeight(); j++) {
					for (size_t i = 0; i < out.getWidth(); i++) {
						out(i, j, k)[0] = applyFilter(in, i, j, k, sampleFreqency, filter);
						out(i, j, k)[1] = 0;
					}
				}	
			}
		}
		
		
		return out;
	}
	
	ooc::Volume createDownsampledOocVolume(ooc::Volume const& in, size_t sampleFreqency) {
		ooc::Volume out(in.getWidth()/sampleFreqency, 
								 in.getHeight()/sampleFreqency,
								 in.getDepth()/sampleFreqency);
		
		auto filter = createAveragingFilter(sampleFreqency);
		
		for (size_t k = 0; k < out.getDepth(); k++) {
			for (size_t j = 0; j < out.getHeight(); j++) {
				for (size_t i = 0; i < out.getWidth(); i++) {
					out(i, j, k) = applyFilter(in, i, j, k, sampleFreqency, filter);
				}
			}	
		}
		
		return out;
	}
	
	void outputVolume(ooc::Volume const& volume, std::string const& output) {
		using namespace boost;
		
		if (filesystem::exists(output)) {
			throw std::runtime_error{output + " already exists."};
		}
		
		filesystem::path dir(output);
		if (!filesystem::create_directories(dir)) {
			throw std::runtime_error{"Failed creating directory " + output};
		}
		
		// TODO: Static assert that VolumeSlice::value_type is 16bit uint
		double rescale = double(std::numeric_limits<VolumeSlice::value_type>::max()) / double(std::numeric_limits<ooc::value_type>::max());
		for (size_t k = 0; k < volume.getDepth(); k++) {
			cv::Mat slice(volume.getHeight(), volume.getWidth(), CV_16UC1);
			for (size_t j = 0; j < volume.getHeight(); j++) {
				for (size_t i = 0; i < volume.getWidth(); i++) {
					slice.at<VolumeSlice::value_type>(j,i) = rescale*static_cast<VolumeSlice::value_type>(volume(i,j,k));
				}
			}
			std::stringstream ss;
			ss << k << ".png";
			std::string filename = ss.str();
			while (filename.size()-4 < 5) {
				filename = "0"+filename;
			}
			assert(cv::imwrite(filename, slice));
		}
	}
	
}}
