#include "optimization.h"

#include <cassert>
#include <iostream>

namespace vdr {
	
	using size_type = Vector::size_type;
	using boost::numeric::ublas::norm_inf;
	
	Vector gradient(Function func, Vector x, Vector h) {
		Vector dx(x.size());
		
		for (size_type i = 0; i < x.size(); i++) {
			Vector v0 = x;
			Vector v1 = x;
			v0[i] += h[i];
			v1[i] -= h[i];
			dx[i] = (func(v0) - func(v1)) / (2*h[i]);
		}
		
		return dx;
	}
	
	Vector gradientDescent(Function func, Vector x, Vector h, 
						   size_t maxIterations, real_t step, real_t tol) {
		assert(x.size() == h.size());
		
		for (size_t itr = 0; itr < maxIterations; itr++) {
			Vector dx = step * gradient(func, x, h);
			x = x - dx;
			if (norm_inf(dx) < tol) {break;}
		}
		
		return x;
	}
	
}
