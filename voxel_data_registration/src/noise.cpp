#include "noise.h"
#include "oocvolume.h"

#include <random>
#include <numeric>

namespace vdr {
	
	template<class T>
	T clamp(T val, T min, T max) {
		return std::min(std::max(val, min), max);
	}
	
	void addGaussianNoise(ooc::Volume & volume, double mean, double stddev) {
		std::random_device randomDevice;
		std::mt19937 generator(randomDevice());
		std::normal_distribution<double> distribution(mean, stddev);
		
		double min = std::numeric_limits<ooc::value_type>::min();
		double max = std::numeric_limits<ooc::value_type>::max();
		
		for(size_t k = 0; k < volume.getDepth(); k++) {
			for(size_t j = 0; j < volume.getHeight(); j++) {
				for(size_t i = 0; i < volume.getWidth(); i++) {
					volume(i, j, k) = clamp(volume(i, j, k) + max*distribution(generator), min, max);
				}
			}
		}
		
	}
	
	
}
