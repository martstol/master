#include "transform.h"
#include "interpolate.h"

#include <glm/glm.hpp>

#include <algorithm>
#include <cmath>

namespace vdr {
	
	void transformCorners(std::array<glm::vec3, 8> & corners, glm::mat4 mat) {
		for(glm::vec3 & v : corners) {
			glm::vec4 u = mat*glm::vec4(v, 1.0f);
			v = glm::vec3(u);
		}
	}

	std::pair<glm::vec3, glm::vec3> findTransformedAABB(std::array<glm::vec3, 8> corners, glm::mat4 mat) {
		transformCorners(corners, mat);
		
		float minX = corners[0].x, maxX = corners[0].x;
		float minY = corners[0].y, maxY = corners[0].y;
		float minZ = corners[0].z, maxZ = corners[0].z;
		
		for(glm::vec3 v : corners) {
			minX = std::min(minX, v.x);
			maxX = std::max(maxX, v.x);
			minY = std::min(minY, v.y);
			maxY = std::max(maxY, v.y);
			minZ = std::min(minZ, v.z);
			maxZ = std::max(maxZ, v.z);
		}
		
		return {glm::vec3{minX, minY, minZ}, glm::vec3{maxX, maxY, maxZ}};
	}
	
	template<class T>
	T allocateOutputVolume(glm::vec3 min, glm::vec3 max) {
		size_t w = std::ceil(max.x - min.x);
		size_t h = std::ceil(max.y - min.y);
		size_t d = std::ceil(max.z - min.z);
		return T(w, h, d);
	}
	
	void resample(array3<fftw_complex> const& in, fftw_complex & out, 
				  glm::vec4 v, glm::mat4 mat) {
		glm::vec4 u = mat*v;
		if (0 <= u.x && u.x <= (in.getWidth()-1) &&
			0 <= u.y && u.y <= (in.getHeight()-1) &&
			0 <= u.z && u.z <= (in.getDepth()-1)) {
			interpolate::trilinear(u.x, u.y, u.z, in, out);
		} else {
			out[0] = out[1] = 0;
		}
	}

	array3<fftw_complex> transform(array3<fftw_complex> const& in, glm::mat4 mat) {
		glm::vec3 min, max;
		std::tie(min, max) = findTransformedAABB(getVolumeCorners(in), mat);
		array3<fftw_complex> out = allocateOutputVolume<array3<fftw_complex>>(min, max);
		
		glm::mat4 inv = glm::inverse(mat);
		
		#pragma omp parallel for
		for (size_t k = 0; k < out.getDepth(); k++) {
			for (size_t j = 0; j < out.getHeight(); j++) {
				for (size_t i = 0; i < out.getWidth(); i++) {
					glm::vec4 v{i+min.x, j+min.y, k+min.z, 1.0f};
					resample(in, out(i, j, k), v, inv);
				}
			}
		}
		
		return out;
	}
	
	ooc::Volume transform(ooc::Volume const& in, glm::mat4 mat) {
		glm::vec3 min, max;
		std::tie(min, max) = findTransformedAABB(getVolumeCorners(in), mat);
		ooc::Volume out = allocateOutputVolume<ooc::Volume>(min, max);
		
		mat = glm::inverse(mat);
		
		for (size_t k = 0; k < out.getDepth(); k++) {
			for (size_t j = 0; j < out.getHeight(); j++) {
				for (size_t i = 0; i < out.getWidth(); i++) {
					glm::vec4 v(i,j,k,1);
					out(i,j,k) = in(mat*v);
				}
			}
		}
		
		return out;
	}

}
