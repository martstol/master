#include "optimization.h"

#include <iostream>
#include <cassert>

namespace vdr {
	
	std::vector<Vector> genNeighbours(Vector x, Vector h) {
		std::vector<Vector> neighbours;
		for (size_t i = 0; i < x.size(); i++) {
			Vector n = x;
			n[i] += h[i];
			neighbours.push_back(n);
			
			n = x;
			n[i] -= h[i];
			neighbours.push_back(n);
		}
		return neighbours;
	}
	
	Vector hillClimbing(Function func, Vector x, Vector h, size_t maxIterations, real_t tol) {
		assert(x.size() == h.size());
		
		real_t best = func(x);
		for (size_t itr = 0; itr < maxIterations; itr++) {
			auto neighbours = genNeighbours(x, h);
			
			bool solutionChanged = false;
			for (Vector n : neighbours) {
				real_t temp = func(n);
				if (temp > best) {
					best = temp;
					x = n;
					solutionChanged = true;
				}
			}
			
			if (!solutionChanged) {
				h = 0.5*h;
				for (auto val : h) {
					if (val < tol) {
						return x;
					}
				}
			}
		}
		return x;
	}
	
}
