# Copyright (c) 2015, Project OSRM contributors
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, this list
# of conditions and the following disclaimer.
# Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# Locate STXXL library
# This module defines
#  STXXL_FOUND, if false, do not try to link to libstxxl
#  STXXL_LIBRARY
#  STXXL_INCLUDE_DIR, where to find stxxl.h
#


IF( NOT STXXL_FIND_QUIETLY )
    MESSAGE(STATUS "Looking for STXXL...")
ENDIF()

FIND_PATH(STXXL_INCLUDE_DIR stxxl.h
  HINTS
  $ENV{STXXL_DIR}
  PATH_SUFFIXES stxxl include/stxxl/stxxl include/stxxl include
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local
  /usr
  /opt/local # DarwinPorts
  /opt
)

FIND_LIBRARY(STXXL_LIBRARY
  NAMES stxxl
  HINTS
  $ENV{STXXL_DIR}
  PATH_SUFFIXES lib64 lib
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local
  /usr
  /opt/local
  /opt
)

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set STXXL_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(STXXL  DEFAULT_MSG  STXXL_LIBRARY STXXL_INCLUDE_DIR)

IF( NOT STXXL_FIND_QUIETLY )
    IF( STXXL_FOUND )
        MESSAGE(STATUS "Found STXXL: ${STXXL_LIBRARY}" )
    ENDIF()
ENDIF()

MARK_AS_ADVANCED(STXXL_INCLUDE_DIR STXXL_LIBRARY)