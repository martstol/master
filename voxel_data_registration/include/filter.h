#pragma once

#ifndef _FILTER_H_
#define _FILTER_H_

#include "array.h"
#include "fftw_complex_math.h"

#include <boost/math/constants/constants.hpp>

#include <cmath>

namespace vdr {
	
	template <class T, class Filter>
	static void applyFilter(array3<T> & array, Filter filter) {
		size_t w = array.getWidth(), h = array.getHeight(), d = array.getDepth();
		for (size_t k = 0; k < array.getDepth(); k++) {
			for (size_t j = 0; j < array.getHeight(); j++) {
				for (size_t i = 0; i < array.getWidth(); i++) {
					array(i, j, k) *= filter(i, j, k, w, h, d);
				}
			}
		}
	}

	template <class Filter>
	static void applyFilter(array3<fftw_complex> & array, Filter filter) {
		size_t w = array.getWidth(), h = array.getHeight(), d = array.getDepth();
		for (size_t k = 0; k < d; k++) {
			for (size_t j = 0; j < h; j++) {
				for (size_t i = 0; i < w; i++) {
					array(i, j, k)[0] = abs(array(i, j, k))*filter(i, j, k, w, h, d);
					array(i, j, k)[1] = 0;
				}
			}
		}
	}
	
	static double highPassFilter(size_t x, size_t y, size_t z, 
						  size_t w, size_t h, size_t d) {
		
		auto pi = boost::math::constants::pi<double>();
		
		double a = x / (double)(w-1) - 0.5;
		double b = y / (double)(h-1) - 0.5;
		double c = z / (double)(d-1) - 0.5;
		a = (std::cos(2.0*pi*a)+1.0) * 0.5;
		b = (std::cos(2.0*pi*b)+1.0) * 0.5;
		c = (std::cos(2.0*pi*c)+1.0) * 0.5;
		return 1.0 - (a*b*c);
	}
	
}

#endif
