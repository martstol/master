#pragma once

#ifndef _INTERPOLATE_H_
#define _INTERPOLATE_H_

#include "array.h"
#include "oocvolume.h"

#include <cmath>

namespace vdr {

	namespace interpolate {
		
		template<class T>
		static double lerp(T v0, T v1, double t) {
			return (1-t)*v0 + t*v1;
		}
		
		template<class T>
		static double linear(double x, array<T> const& array) {
			size_t i0 = floor(x);
			size_t i1 = ceil(x);
			return lerp(array(i0), array(i1), x - i0);
		}
		
		template<class T>
		static double bilinear(double x, double y, array2<T> const& array) {
			size_t i0 = floor(x);
			size_t i1 = ceil(x);
			size_t j0 = floor(y);
			size_t j1 = ceil(y);
			double v0 = lerp(array(i0, j0), array(i1, j0), x - i0);
			double v1 = lerp(array(i0, j1), array(i1, j1), x - i0);
			return lerp(v0, v1, y - j0);
		}
		
		template<class T>
		static double trilinear(double x, double y, double z, array3<T> const& array) {
			size_t i0 = floor(x);
			size_t i1 = ceil(x);
			size_t j0 = floor(y);
			size_t j1 = ceil(y);
			size_t k0 = floor(z);
			size_t k1 = ceil(z);
			double v00 = lerp(array(i0, j0, k0), array(i1, j0, k0), x - i0);
			double v10 = lerp(array(i0, j1, k0), array(i1, j1, k0), x - i0);
			double v01 = lerp(array(i0, j0, k1), array(i1, j0, k1), x - i0);
			double v11 = lerp(array(i0, j1, k1), array(i1, j1, k1), x - i0);
			double u0 = lerp(v00, v10, y - j0);
			double u1 = lerp(v01, v11, y - j0);
			return lerp(u0, u1, z - k0);
		}
		
		static double trilinear(double x, double y, double z, ooc::Volume const& volume) {
			size_t i0 = floor(x);
			size_t i1 = ceil(x);
			size_t j0 = floor(y);
			size_t j1 = ceil(y);
			size_t k0 = floor(z);
			size_t k1 = ceil(z);
			double v00 = lerp(volume(i0, j0, k0), volume(i1, j0, k0), x - i0);
			double v10 = lerp(volume(i0, j1, k0), volume(i1, j1, k0), x - i0);
			double v01 = lerp(volume(i0, j0, k1), volume(i1, j0, k1), x - i0);
			double v11 = lerp(volume(i0, j1, k1), volume(i1, j1, k1), x - i0);
			double u0 = lerp(v00, v10, y - j0);
			double u1 = lerp(v01, v11, y - j0);
			return lerp(u0, u1, z - k0);
		}
		
		static void trilinear(double x, double y, double z, array3<fftw_complex> const& in, 
							  fftw_complex & out) {
			size_t i0 = floor(x);
			size_t i1 = ceil(x);
			size_t j0 = floor(y);
			size_t j1 = ceil(y);
			size_t k0 = floor(z);
			size_t k1 = ceil(z);
			for (int i = 0; i < 2; i++) {
				double v00 = lerp(in(i0, j0, k0)[i], in(i1, j0, k0)[i], x - i0);
				double v10 = lerp(in(i0, j1, k0)[i], in(i1, j1, k0)[i], x - i0);
				double v01 = lerp(in(i0, j0, k1)[i], in(i1, j0, k1)[i], x - i0);
				double v11 = lerp(in(i0, j1, k1)[i], in(i1, j1, k1)[i], x - i0);
				double u0 = lerp(v00, v10, y - j0);
				double u1 = lerp(v01, v11, y - j0);
				out[i] = lerp(u0, u1, z - k0);
			}
			
		}
	
	}

}

#endif