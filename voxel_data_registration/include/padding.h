#pragma once

#ifndef _PADDING_H_
#define _PADDING_H_

#include "array.h"

#include <fftw3.h>

namespace vdr {
	array3<fftw_complex> pad(array3<fftw_complex> const& in, size_t dx, size_t dy, size_t dz);
	void padVolumes(array3<fftw_complex> & a, array3<fftw_complex> & b);
}

#endif