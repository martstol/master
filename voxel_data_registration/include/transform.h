#pragma once

#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "array.h"
#include "oocvolume.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <type_traits>
#include <array>
#include <utility>

namespace vdr {
	
	template <class T>
	T degToRad(T deg) {
		static_assert(std::is_floating_point<T>::value, "Template must be a floating point type");
		return glm::pi<T>()*deg / 180.0;
	}
	
	static std::array<glm::vec3, 8> getVolumeCorners(size_t w, size_t h, size_t d) {
		return std::array<glm::vec3, 8>{
			glm::vec3{0, 0, 0},
			glm::vec3{w, 0, 0},
			glm::vec3{0, h, 0},
			glm::vec3{0, 0, d},
			glm::vec3{w, h, 0},
			glm::vec3{w, 0, d},
			glm::vec3{0, h, d},
			glm::vec3{w, h, d},
		};
	}
	
	template <class T>
	std::array<glm::vec3, 8> getVolumeCorners(T const& t) {
		return getVolumeCorners(t.getWidth(), t.getHeight(), t.getDepth());
	}
	
	std::pair<glm::vec3, glm::vec3> findTransformedAABB(std::array<glm::vec3, 8> corners, glm::mat4 mat);
	array3<fftw_complex> translate(array3<fftw_complex> const& in, double dx, double dy, double dz);
	array3<fftw_complex> transform(array3<fftw_complex> const& in, glm::mat4 mat);
	ooc::Volume transform(ooc::Volume const& in, glm::mat4 mat);
	
}
	
#endif