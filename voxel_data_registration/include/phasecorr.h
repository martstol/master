#pragma once

#ifndef _PHASECORRELATION_H_
#define _PHASECORRELATION_H_

#include "oocvolume.h"

#include <glm/glm.hpp>

namespace vdr { namespace phasecorr {
	
	glm::mat4 registerTransformation(ooc::Volume const& fixed, ooc::Volume const& moving, size_t sampleFrequency, size_t maxIterations);
	
}}

#endif