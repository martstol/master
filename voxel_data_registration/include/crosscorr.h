#pragma once

#ifndef _CROSSCORR_H_
#define _CROSSCORR_H_

#include "oocvolume.h"

#include <string>
#include <glm/glm.hpp>

namespace vdr { namespace crosscorr {
	
	glm::mat4 registerTransformation(ooc::Volume const& fixed, ooc::Volume const& moving, float cutFraction, size_t maxIterations);
	
}}

#endif