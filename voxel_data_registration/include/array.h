#pragma once

#ifndef _ARRAY_H_
#define _ARRAY_H_

#include <fftw3.h>

#include <cassert>
#include <iostream>
#include <algorithm>

namespace vdr {
	
	template<class T>
	class array final {
	private:
		T * content;
		size_t n;
		
	public:
		explicit array(size_t size) : n(size), content((T*)fftw_malloc(sizeof(T)*n)) {}
		~array() {fftw_free(content);}
		
		array(array const& other) = delete;
		array& operator = (array const& other) = delete;
		
		array(array && other) : content(other.content), n(other.n) {
			other.content = nullptr;
			other.n = 0;
		}
		
		array& operator = (array && other) {
			fftw_free(content);
			content = other.content;
			n = other.n;
			
			other.content = nullptr;
			other.n = 0;
			
			return *this;
		}
		
		T& operator [] (size_t i) {
			return content[i];
		}
		
		T const& operator [] (size_t i) const {
			return content[i];
		}
		
		T& operator () (size_t i) {
			return content[i];
		}
		
		T const& operator () (size_t i) const {
			return content[i];
		}
		
		size_t size() const {
			return n;
		}
		
		T * data() {
			return content;
		}
		
		T const * data() const {
			return content;
		}
		
		array copy() const {
			array<T> out{n};
			std::copy(&content[0], &content[size()], out.content);
			return out;
		}
		
	};
	
	template<class T>
	class array2 final {
	private:
		T * content;
		size_t width;
		size_t height;
		
		size_t index(size_t x, size_t y) const {
			return x + y * width;
		}
		
	public:
		explicit array2(size_t width, size_t height) : 
			content((T*)fftw_malloc(sizeof(T)*width*height)),
			width(width), height(height) {}
		~array2() {fftw_free(content);}
		
		array2(array2 const& other) = delete;
		array2& operator = (array2 const& other) = delete;
		
		array2(array2 && other) : content(other.content),
			width(other.width), height(other.height) {
			other.content = nullptr;
			other.width = 0;
			other.height = 0;
		}
		
		array2& operator = (array2 && other) {
			fftw_free(content);
			content = other.content;
			width = other.width;
			height = other.height;
			
			other.content = nullptr;
			other.width = 0;
			other.height = 0;
			
			return *this;
		}
		
		T& operator [] (size_t i) {
			return content[i];
		}
		
		T const& operator [] (size_t i) const {
			return content[i];
		}
		
		T& operator () (size_t x, size_t y) {
			return content[index(x, y)];
		}
		
		T const& operator () (size_t x, size_t y) const {
			return content[index(x, y)];
		}
		
		size_t getWidth() const {
			return width;
		}
		
		size_t getHeight() const {
			return height;
		}
		
		size_t size() const {
			return width*height;
		}
		
		T * data() {
			return content;
		}
		
		T const * data() const {
			return content;
		}
		
		array2 copy() const {
			array2<T> out{width, height};
			std::copy(&content[0], &content[size()], out.content);
			return out;
		}
		
	};
	
	template<class T>
	class array3 final {
	private:
		T * content;
		size_t width;
		size_t height;
		size_t depth;
		
		size_t index(size_t x, size_t y, size_t z) const {
			return x + y * width + z * width * height;
		}
		
	public:
		explicit array3(size_t width, size_t height, size_t depth) :
			content((T*)fftw_malloc(sizeof(T)*width*height*depth)),
			width(width), height(height), depth(depth) {}
		
		~array3() {fftw_free(content);}
		
		array3(array3 const& other) = delete;
		array3& operator = (array3 const& other) = delete;
		
		array3(array3 && other) : content(other.content), 
			width(other.width), height(other.height), depth(other.depth) {
			other.content = nullptr;
			other.width = 0;
			other.height = 0;
			other.depth = 0;
		}
		
		array3& operator = (array3 && other) {
			fftw_free(content);
			content = other.content;
			width = other.width;
			height = other.height;
			depth = other.depth;
			
			other.content = nullptr;
			other.width = 0;
			other.height = 0;
			other.depth = 0;
			
			return *this;
		}
		
		T& operator [] (size_t i) {
			return content[i];
		}
		
		T const& operator [] (size_t i) const {
			return content[i];
		}
		
		T& operator () (size_t x, size_t y, size_t z) {
			return content[index(x, y, z)];
		}
		
		T const& operator () (size_t x, size_t y, size_t z) const {
			return content[index(x, y, z)];
		}
		
		size_t getWidth() const {
			return width;
		}
		
		size_t getHeight() const {
			return height;
		}
		
		size_t getDepth() const {
			return depth;
		}
		
		size_t size() const {
			return width*height*depth;
		}
		
		T * data() {
			return content;
		}
		
		T const * data() const {
			return content;
		}
		
		array3 copy() const {
			array3<T> out{width, height, depth};
			std::copy(&content[0], &content[size()], out.content);
			return out;
		}
		
	};
	
}

#endif
