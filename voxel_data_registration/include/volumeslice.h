#pragma once

#ifndef _VOLUME_SLICE_H_
#define _VOLUME_SLICE_H_

#include <boost/filesystem.hpp>
#include <dcmtk/dcmimgle/dcmimage.h>

#include <memory>
#include <string>
#include <cstdint>

namespace vdr {
	
	class VolumeSlice {
	private:
		boost::filesystem::path path;
		std::shared_ptr<DicomImage> dicomImage;
		
	public:
		
		using value_type = uint16_t;
		
		explicit VolumeSlice() : path(""), dicomImage(nullptr) {}; 
		explicit VolumeSlice(boost::filesystem::path const& filePath);
		
		size_t numRows() const;
		size_t numColumns() const;
		value_type at(size_t row, size_t column) const;
		value_type operator()(size_t row, size_t column) const;
		value_type operator[](size_t i) const;
		
	};
	
}

#endif