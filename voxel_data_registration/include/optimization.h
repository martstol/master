#pragma once

#ifndef _OPTIMIZATION_H_
#define _OPTIMIZATION_H_

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <functional>
#include <vector>

namespace vdr {
	
	using real_t = float;
	using Matrix = boost::numeric::ublas::matrix<real_t>;
	using Vector = boost::numeric::ublas::vector<real_t, std::vector<real_t>>;
	using Function = std::function<real_t(Vector)>;
	
	Vector gradient(Function func, Vector x, Vector h);
	
	Vector newtonsMethod(Function func, Vector x, Vector h,
						 size_t maxIterations, real_t step, real_t tol);
	
	Vector gradientDescent(Function func, Vector x, Vector h, 
						   size_t maxIterations, real_t step, real_t tol);
	
	Vector hillClimbing(Function func, Vector x, Vector h, 
						size_t maxIterations, real_t tol);
	
}

#endif