#pragma once

#ifndef _VDR_H_
#define _VDR_H_

#include <string>

namespace vdr {

	void voxelDataRegistration(std::string const& filepath1, std::string const& filepath2,
							   size_t downscaleFilterSize, size_t phaseOneMaxItr,
							   double subvolumeCut, size_t phaseTwoMaxItr,
							   bool output = false, std::string const& outputPath = "");

}

#endif