#pragma once

#ifndef _SHOW_VOLUME_H_
#define _SHOW_VOLUME_H_

#include "array.h"
#include "axis.h"
#include "oocvolume.h"

#include <fftw3.h>
#include <glm/glm.hpp>

#include <string>

namespace vdr {
	
	void showVolumeSlice(array3<fftw_complex> const& volume, size_t slice, Axis axis);
	void storeVolumeSlice(array3<fftw_complex> const& volume, size_t slice, Axis axis, std::string const& path);
	void outputVolumeSlices(array3<fftw_complex> const& volume, std::string const& path, size_t frequency = 10);
	void showVolumeSlice(ooc::Volume const& volume, size_t slice, glm::mat4 mat = glm::mat4(1.0f), Axis axis = Z_AXIS, bool output = false);
	void showVolumeSliceDiff(ooc::Volume const& fixed, ooc::Volume const& moving, size_t slice0, size_t slice1, glm::mat4 mat, Axis axis, bool output = false, std::string path = "");
	
}

#endif