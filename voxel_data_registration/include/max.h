#pragma once

#ifndef _MAX_H_
#define _MAX_H_

#include "array.h"

#include <array>

namespace vdr {
	std::array<size_t, 3> find_max(array3<fftw_complex> const& array);
}

#endif