#pragma once

#ifndef _FFT_H_
#define _FFT_H_

#include "array.h"

#include <fftw3.h>

#include <iostream>
#include <cassert>
#include <utility>

namespace vdr {
	
	static array3<fftw_complex> fftshift(array3<fftw_complex> const& in) {
		size_t w = in.getWidth(), h = in.getHeight(), d = in.getDepth();
		array3<fftw_complex> out(w, h, d);
		
		#pragma omp parallel for
		for (size_t k = 0; k < d; k++) {
			for (size_t j = 0; j < h; j++) {
				for (size_t i = 0; i < w; i++) {
					size_t u = (i + w / 2) % w;
					size_t v = (j + h / 2) % h;
					size_t w = (k + d / 2) % d;
					out(u, v, w)[0] = in(i, j, k)[0];
					out(u, v, w)[1] = in(i, j, k)[1];
				}
			}
		}
		return out;
	}
	
	static void fft(array3<fftw_complex> & array) {
		fftw_plan plan = fftw_plan_dft_3d(array.getDepth(), array.getHeight(), array.getWidth(),
										  array.data(), array.data(),
										  FFTW_FORWARD, FFTW_ESTIMATE);
		fftw_execute(plan);
		fftw_destroy_plan(plan);
	}
	
	static void ifft(array3<fftw_complex> & array, bool normalize = true) {
		fftw_plan plan = fftw_plan_dft_3d(array.getDepth(), array.getHeight(), array.getWidth(),
										  array.data(), array.data(),
										  FFTW_BACKWARD, FFTW_ESTIMATE);
		fftw_execute(plan);
		fftw_destroy_plan(plan);
		if (normalize) {
			#pragma omp parallel for
			for (size_t i = 0; i < array.size(); i++) {
				array[i][0] /= (double)array.size();
				array[i][1] /= (double)array.size();
			}
		}
	}
	
}

#endif
