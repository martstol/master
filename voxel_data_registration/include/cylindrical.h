#pragma once

#ifndef _CYLINDRICAL_H_
#define _CYLINDRICAL_H_

#include "array.h"
#include "axis.h"

namespace vdr {
	
	array3<fftw_complex> toCylindricalCoordinates(array3<fftw_complex> const& in, Axis axis);
	
}

#endif