#pragma once

#ifndef _AXIS_H_
#define _AXIS_H_

#include <array>
#include <string>

#include <glm/glm.hpp>

namespace vdr {
	enum Axis {X_AXIS=0, Y_AXIS=1, Z_AXIS=2};
	static const std::array<std::string, 3> AXIS_NAMES{"X_AXIS", "Y_AXIS", "Z_AXIS"};
	static const std::array<glm::vec3, 3> AXIS_VECTOR{glm::vec3(1.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f), glm::vec3(0.f, 0.f, 1.f)};
	static const std::array<Axis, 3> INDEXED_AXISES{X_AXIS, Y_AXIS, Z_AXIS};
}

#endif