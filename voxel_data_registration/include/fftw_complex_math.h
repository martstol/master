#pragma once

#ifndef _FFTW_COMPLEX_MATH_H_
#define _FFTW_COMPLEX_MATH_H_

#include <fftw3.h>

namespace vdr {
	double abs(fftw_complex const& c);
}

#endif