#pragma once

#ifndef _VDR_VOLUMEFILES_H_
#define _VDR_VOLUMEFILES_H_

#include "volumeslice.h"
#include "array.h"

#include <fftw3.h>
#include <boost/filesystem.hpp>

#include <string>
#include <vector>
#include <unordered_map>

namespace vdr {
	
	static std::string const DCM_FILE_EXTENTION{".dcm"};
	
	class VolumeFiles {
	private:
		boost::filesystem::path directory;
		std::vector<boost::filesystem::path> fileList;
		
	public:
		explicit VolumeFiles(boost::filesystem::path const& directoryPath);
		size_t numFiles() const;
		VolumeSlice loadVolumeSlice(size_t slice) const;
		
	};
}

#endif