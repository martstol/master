#pragma once

#ifndef _OOCVOLUME_H_
#define _OOCVOLUME_H_

#include "array.h"

#include <stxxl/vector>
#include <glm/glm.hpp>

#include <cinttypes>
#include <string>
#include <memory>

namespace vdr { namespace ooc {
	
	using value_type = uint8_t;
	size_t const page_size = 8;
	size_t const cache_pages = 128*1024;
	size_t const block_size = 4096;
	using Vector = stxxl::VECTOR_GENERATOR<value_type, page_size, cache_pages, block_size>::result;
	
	class Volume {
	private:
		size_t width, height, depth;
		std::shared_ptr<Vector> vector;
		
		size_t index(size_t i, size_t j, size_t k) const;
		
	public:
		explicit Volume(std::string const& filepath);
		explicit Volume(size_t w, size_t h, size_t d);
		
		value_type & operator()(size_t i, size_t j, size_t k);
		value_type const& operator()(size_t i, size_t j, size_t k) const;
		value_type operator()(glm::vec4 vec) const;
		value_type operator()(glm::vec3 vec) const;
		
		size_t getWidth() const {return width;}
		size_t getHeight() const {return height;}
		size_t getDepth() const {return depth;}
		glm::vec3 size() const {return glm::vec3(width, height, depth);}
		
	};
	
	array3<fftw_complex> createDownsampledVolume(ooc::Volume const& in, size_t sampleFreqency);
	ooc::Volume createDownsampledOocVolume(ooc::Volume const& in, size_t sampleFreqency);
	
	void outputVolume(ooc::Volume const& volume, std::string const& output);
	
}}

#endif
