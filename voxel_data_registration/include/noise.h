#pragma once

#ifndef _NOISE_H_
#define _NOISE_H_

#include "oocvolume.h"

namespace vdr {

	void addGaussianNoise(ooc::Volume & volume, double mean = 0, double stddev = 0.1);

}

#endif
