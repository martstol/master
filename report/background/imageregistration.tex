\section{Image registration}
\label{sec:imgreg}

Image registration is the process of aligning two images such that corresponding
points in the images correspond to the same location in the origin of the images.
The images can be recorded at different times, from different viewpoints or by
different sensors. Image registration is an important task in image
processing which is often performed before further processing of the images can
take place.

Image registration methods can be classified according to how the images were
acquired. The four main classes are \emph{multi-modal registration},
\emph{template matching}, \emph{viewpoint registration} and \emph{temporal
registration}. \emph{Multi-modal registration} is to register images acquired by
different types of sensors with the objective of combining the information acquired
from the different sensors. For example to combine medical images obtained from
magnetic resonance imaging, ultrasound or x-ray computerized tomography. In
\emph{template matching} the objective is to find the optimal location and
orientation of a smaller template image in a target image. \emph{Viewpoint
registration} uses images of the same scene from different viewpoints to
gain a larger two- or three-dimensional view of the scene. In \emph{temporal
registration}, images of the same scene are acquired at different points in time.
The objective is to evaluate the changes in the scene. This can for example be used
in medical evaluation.

An image registration method consists of the following four components, a
\emph{feature space}, a \emph{search space}, a \emph{search strategy} and a
\emph{similarity metric}. The \emph{feature space} decides how information is
extracted from the image. The \emph{search space} is the class of
transformations required to align the images. The \emph{search strategy} decides
how to select the next transformation and the \emph{similarity metric}
determines the quality of the alignment.

There are several types of distortions that may increase the complexity of image
registration. Examples are sensor noise, changes in the recorded object such as
movements, deformations or growths, lighting and atmospheric changes or
differences in the sensors used to record the object\cite{Brown:1992:SIR:146370.146374}.

Presented here are some image registration methods that have been considered
for this thesis. The methods are cross-correlation, sequential methods, mutual
information methods, feature methods and
Fourier methods. The method chosen to be implemented and the reason for choosing
that method is described in chapter \ref{chap:implementation}.

\subsection{Cross-correlation}

The cross-correlation method is based on the similarity metric of the same name.
The feature space of cross correlation is the intensity value of the pixels. The
search space consists of all rigid transformations. Cross-correlation can be
considered as a typical optimization problem, for simple transformations like
translations it is feasible to compute the similarity metric for every
transformation, however this quickly becomes infeasible for three-dimensional
images and more complicated transformations like rotation and scaling. 
Cross-correlation is best suited for template matching. The
cross-correlation of two images $f_1$ and $f_2$ for every applicable
transformation $\mathbf{T}$ is defined according to the following equation.

\begin{equation*}
	C(\mathbf{T}) = \frac{
		\sum_{\mathbf{x}} f_1(\mathbf{x})f_2(\mathbf{Tx})
	}
	{
		\sqrt{
			\sum_{\mathbf{x}} f_1(\mathbf{x})^2 \sum_{\mathbf{x}} f_2(\mathbf{Tx})^2
		}
	}
\end{equation*}

where $\mathbf{x}$ is a vector representing an image location and $\mathbf{T}$
is a transformation matrix. If $f_2$ is equal to $f_1$ transformed by
$\mathbf{T_0}$, then the cross-correlation will have a global maximum when
$\mathbf{T} = \mathbf{T_0}$. There are several other versions of the cross-
correlation function, all of them based on the same idea. Figure
\ref{fig:crosscorrelation_illustrated} illustrates the cross correlation
function of two images for all translations. The cross-correlation was calculated using matlab's
normxcorr2\footnote{\url{http://se.mathworks.com/help/images/ref/normxcorr2.html}}.

\begin{figure}[htbp]
	\centering

	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/lena1.png}
		\caption{Lena cropped.}
		\label{fig:lena_cropped_cc}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/lena2.png}
		\caption{Lena cropped and shifted.}
		\label{fig:lena_cropped_shifted_cc}
	\end{subfigure}

	\begin{subfigure}[b]{1.0\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/crosscorr.png}
		\caption{Cross correlation function.}
		\label{fig:crosscorrelation}
	\end{subfigure}

	\caption{Two cropped versions of Lena. The image in figure
	\ref{fig:lena_cropped_shifted_cc} is shifted relative to figure \ref{fig:lena_cropped_cc}.
	Figure \ref{fig:crosscorrelation} is the cross-correlation function of the two
	images for all translation registrations.}
	\label{fig:crosscorrelation_illustrated}
\end{figure}

The images can be registered by computing the cross-correlation for every
transformation. This approach is only viable when the allowable transformations
are limited to a small amount of rotation, translation and scaling transformations,
because as the number of transformations grow, the computational cost grows rapidly.
Additionally cross-correlation is sensitive to variations in pixel intensity caused
by for example noise, different illumination or use of different sensor types. 
This can cause the cross-correlation's maximum to be challenging to identify
correctly and there can be several local maximums, complicating the search
strategy\cite{Zitova2003977}.

\subsection{Sequential methods}

Sequential similarity detection algorithms (SSDAs) are very similar to cross-correlation.
They employ a simpler and more efficient similarity measure than cross-correlation,
using the absolute value of the difference between the two images.
The feature space, search space and search strategy are the same as when using
the cross-correlation method. The standard definition of the similarity measure
is the sum of absolute differences.

\begin{equation*}
	E(\mathbf{T}) = \sum_{\mathbf{x}}|f_1(\mathbf{x}) - f_2(\mathbf{Tx})|
\end{equation*}

Or the normalized measure, where the average intensity value of the images is
subtracted from each pixel value.

\begin{equation*}
	E(\mathbf{T}) = \sum_{\mathbf{x}}|(f_1(\mathbf{x}) - F_1) - (f_2(\mathbf{Tx}) - F_2)|
\end{equation*}

Where $F_1$ is the average intensity value of $f_1$ and $F_2$ is the average
intensity value of $f_2$. This metric isn't normalized between 0 and 1 but a
global minimum is still guaranteed for a perfect match.
While a lot more efficient, SSDAs are less accurate than cross-correlation and
suffer from the same drawbacks\cite{Brown:1992:SIR:146370.146374}.

\subsection{Mutual information methods}

Mutual information (MI) methods are based on a statistical measure of the dependency
between two data sets called mutual information from information theory. They
work very well for multi-modal image registration which is a challenging task
often required in medical imaging\cite{Zitova2003977}. Mutual information is
based on the joint entropy of the images and attempt to maximize the mutual
information\cite{466930}.

\subsection{Feature methods}

Feature methods attempt to extract some higher level information about the
image, instead of just considering the intensity levels of individual pixels.
Some examples of features are significant regions, lines or points. The next
step is to identify pairs of features between the two images. Features like
regions or lines are often represented by a single point, calculated from for
example the center of gravity, these points are called control points. Feature
methods are usually applied when the type of misalignment is unknown, and is
typically used for viewpoint registration. If the images come from different
viewpoints then the images will be affected by perspective distortions. Because
there is no depth information in the images, feature methods uses control points
appearing in both images to estimate the depth. The search space of feature
methods can be rigid transformations or more complicated transformations.
Feature methods normally use a cost function like mean squared error as
similarity metrics and the search strategy is formulated as a general
optimization problem\cite{Zitova2003977, Brown:1992:SIR:146370.146374}.
An advantages of feature methods is that they can be used for a wide range of
different transformations, including projections, and compress the image
data into a limited set of control points. The main disadvantage with feature
methods is that it is challenging to correctly identify pairs of control points,
especially when the images are subject to noise, and to chose the correct methods
for determining control points.

\subsection{Fourier methods}

Fourier methods differ from other methods in that the feature space is the
frequency domain of the image, this makes these methods robust against
correlated and frequency-dependent noise\cite{Brown:1992:SIR:146370.146374}. The
Fourier transform can be computed efficiently using the Fast Fourier Transform 
(FFT).

Phase correlation is a Fourier method for image registration. The feature space
of the phase correlation method is the frequency domain of the images. The search
space is translations, but it can be extended to work with rotations and scaling.
Phase correlation computes the transformations directly, and doesn't require a
specific search strategy. Phase correlation also works as
the similarity metric, as the phase correlation of two images is a unit impulse
function, which for a perfect match is 0 everywhere except for at a peak corresponding
to the translation between the two images. If the two images does not match
perfectly the peak will have a value lower than 1. The phase correlation method
relies on the Fourier Shift Theorem. Given two images $f_1$ and $f_2$ where $f_2$
is $f_1$ translated by $(x_0, y_0)$:

\begin{equation*}
	f_2(x, y) = f_1(x - x_0, y - y_0)
\end{equation*}

Their Fourier transforms $F_1$ and $F_2$ will be related by:

\begin{equation*}
	F_2(u, v) = e^{-i 2 \pi (u x_0 + v y_0)} F_1(u, v)
\end{equation*}

The two images have the same Fourier magnitude but a different phase related by
their displacements. The cross-power spectrum of $F_1$ and $F_2$ is defined according
to the following equation.

\begin{equation*}
	\frac{F_1(u, v)F_2^{*}(u, v)}{|F_1(u, v)F_2^{*}(u, v)|}
	= e^{- 2 i \pi (u x_0 + v y_0)}
\end{equation*}

Here $F_2^*$ is the complex conjugate of $F_2$. The cross power spectrum is only
defined for images of equal size. If the dimensions of the images does not match,
the images must be padded with 0. Computing the inverse Fourier
transform of the cross-power spectrum gives the phase correlation function for
the two images, which is an impulse function that is approximately 0 everywhere
except at the displacement $(x_0, y_0)$\cite{Brown:1992:SIR:146370.146374}.
Figure \ref{fig:impulse} illustrates the phase correlation function
of two images. The figure has a clear peak corresponding to the translation.
If the value of $x_0$ larger than $image width / 2$ or $y_0$ is larger than
$image height / 2$, then this means that the correct translation is negative.
This is calculated by $x = image width - x_0$ or $y = image height - y_0$\cite{506761}.

\begin{figure}[htbp]
	\centering

	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/lena1.png}
		\caption{Lena cropped.}
		\label{fig:lena_cropped}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/lena2.png}
		\caption{Lena cropped and shifted.}
		\label{fig:lena_cropped_shifted}
	\end{subfigure}

	\begin{subfigure}[b]{1.0\textwidth}
		\includegraphics[width = 1.0\textwidth]{images/imreg/phasecorr.png}
		\caption{Phase correlation function.}
		\label{fig:impulse}
	\end{subfigure}

	\caption{Two cropped versions of Lena. The image in figure
	\ref{fig:lena_cropped_shifted} is shifted relative to figure \ref{fig:lena_cropped}.
	The narrow, red spike in figure \ref{fig:impulse} corresponds to the
	translation of the image in figure \ref{fig:lena_cropped} to the image in
	figure \ref{fig:lena_cropped_shifted}.}
	\label{fig:lena_transformed}
\end{figure}

\subsubsection{Applied to rotation}

Phase correlation can be extended to register images which are both rotated and
translated. Rotation can be registered in the same way as translation by
representing the rotation as a translation with polar coordinates. Polar
coordinates are explained in section \ref{sec:polarcoordinates}. If the image $f_2$
is equal to $f_1$ rotated by the angle $\alpha$ and translated by $x_0, y_0$
then the following equation describes the relationship between $f_1$ and $f_2$.

\begin{equation*}
	f_2(x, y)
	= f_1(x\cos\alpha - y\sin\alpha - x_0, x\sin\alpha + y\cos\alpha - y_0)
\end{equation*}

The Fourier transforms of $f_1$ and $f_2$ are related by the following equation.

\begin{equation*}
	F_2(u, v)
	= e^{-i 2 \pi (u x_0 + v y_0)} F_1(u\cos\alpha - v\sin\alpha,
	u\sin\alpha + v\cos\alpha)
\end{equation*}

This equation depends on both the translation and rotation. However the absolute
value of $F_1$ and $F_2$ depends only upon the rotation.

\begin{equation*}
	|F_2(u, v)|
	= |F_1(u\cos\alpha - v\sin\alpha, u\sin\alpha + v\cos\alpha)|
\end{equation*}

This is because $e^{-i 2 \pi (u x_0 + v y_0)}$ has an absolute value of 1. Then
by expressing $|F_1|$ and $|F_2|$ in polar coordinates the method used to find
the translation estimate can be used to estimate the rotation $\alpha$.

\begin{equation*}
	F_{1 p}(\rho, \theta) = |F_1(\rho\cos\theta, \rho\sin\theta)|
\end{equation*}
\begin{equation*}
	F_{2 p}(\rho, \theta) = |F_2(\rho\cos\theta, \rho\sin\theta)|
\end{equation*}
\begin{equation*}
	|F_2(\rho\cos\theta, \rho\sin\theta)|
	= |F_1(\rho\cos(\theta - \alpha), \rho\sin(\theta - \alpha))|
\end{equation*}
\begin{equation*}
	F_{2 p}(\rho, \theta) = F_{1 p}(\rho, \theta - \alpha)
\end{equation*}

It is important to note that these are periodic functions of the polar coordinate 
$\theta$\cite{387491}.

\begin{equation*}
	\begin{array}{l l}
		F_{1 p}(\rho, \theta) = F_{1 p}(\rho, \theta + n\pi) &
		\quad \text{for $n$ = $\cdots, -2, -1, 0, 1, 2, \cdots$}
	\end{array}
\end{equation*}

\subsubsection{Applied to scaling}

When a uniform scaling factor $s$ is introduced in the equation, the same
approach as for rotation can be used.

\begin{equation*}
	f_2(x, y)
	= f_1(s(x\cos\alpha-y\sin\alpha) - x_0, s(x\sin\alpha+y\cos\alpha) - y_0)
\end{equation*}

The Fourier transforms of the images are related by the following expression:

\begin{equation*}
	F_2(u, v)
	= e^{-i 2 \pi (u x_0 + v y_0)} s^{-2} F_1(s^{-1}(u\cos\alpha - v\sin\alpha),
	s^{-1}(u\sin\alpha + v\cos\alpha))
\end{equation*}

\begin{equation*}
	|F_2(u, v)|
	= s^{-2}|F_1(s^{-1}(u\cos\alpha - v\sin\alpha),
	s^{-1}(u\sin\alpha + v\cos\alpha))|
\end{equation*}

Scaling in the spatial-domain by a factor of $s$ corresponds to a scaling of the
frequency domain by a factor of $s^{-1}$ and changing the amplitude by a factor
of $s^{-2}$. The scaling and rotation are decoupled when the absolute values of
the Fourier transforms are defined in polar coordinates:

\begin{equation*}
	F_{1 p}(\rho, \theta) = |F_1(\rho\cos\theta, \rho\sin\theta)|
\end{equation*}
\begin{equation*}
	F_{2 p}(\rho, \theta) = |F_2(\rho\cos\theta, \rho\sin\theta)|
\end{equation*}
\begin{equation*}
	F_{2 p}(\rho, \theta) = s^{-2}F_{1 p}(\rho / s, \theta - \alpha)
\end{equation*}

Scaling of $f_2$ by a factor of $s$ is the same as scaling the radial coordinate
with $1/s$. The scaling can
be changed into a translation by using a logarithmic scale for the radial
coordinate. 

\begin{equation*}
	F_{1 pl}(\lambda, \theta) = F_{1 p}(\rho, \theta)
\end{equation*}
\begin{equation*}
	F_{2 pl}(\lambda, \theta) = F_{2 p}(\rho, \theta)
\end{equation*}

Where $\lambda = \log(\rho)$. The equation relating $F_{1 pl}(\lambda, \theta)$
and $F_{2 pl}(\lambda, \theta)$ then becomes as follows\cite{387491}:

\begin{equation*}
	F_{2 pl}(\lambda, \theta) = s^{-2}F_{1 pl}(\lambda - \kappa, \theta - \alpha)
\end{equation*}

Where $\kappa = \log(s)$.

When applying phase correlation to register images that have been rotated, scaled
and translated, the rotation and scaling is registered first, independently from
the translation. The transformed image then has the scaling and rotation
reversed and finally the translation is registered.

An additional detail that
has to be taken into account when registering the rotation is that it is
ambiguous if the rotation is $\alpha$ or $\alpha + \pi$. This can be resolved by
applying the inverse of both rotations to two copies of the transformed image
and then doing phase correlation for translation. The phase correlation function
for the correctly rotated image will be closest to a unit impulse function\cite{506761}.
