\section{Downscaling}
\label{sec:downscale}

During the first registration phase, the voxel datasets are downscaled to enable
in-core processing. This section describes two re-sampling methods used for the
downscaling process, compares them and concludes with which of
them produces the best results when used for phase one of the registration method.
It is important for the phase-correlation to select the filter which best
preserves the information in the original dataset when downscaling.

\subsection{Box filter}

A box filter assigns the value of each voxel to the average value of the neighboring
voxels. Figure \ref{fig:downsample_average} shows the result of downscaling a
PRESIOUS stone with a box filter. After the downscaling, the noise present
in the datasets is no longer apparent.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{images/downsampling/averaging.png}
	\caption{Elefsis Large 1 downscaled with a 5x5x5 averaging
	filter.}
	\label{fig:downsample_average}
\end{figure}

\subsection{Gaussian filter}

A Gaussian filter uses the Gaussian distribution to determine the weights of the
neighboring voxels.

\begin{equation*}
	G(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\mathrm{e}^{-\frac{x^2}{2\sigma^2}}
\end{equation*}

$\sigma$ is the standard deviation of the Gaussian distribution, and determines
the width of the Gaussian function. The three-dimensional Gaussian function is
expressed as the product of three Gaussian functions.

\begin{equation*}
	G(x, y, z) = G(x)G(y)G(z) = \frac{1}{\sqrt{2\pi\sigma^2}^3}\mathrm{e}^{-\frac{x^2 + y^2 + z^2}{2\sigma^2}}
\end{equation*}

The Gaussian filter is created by sampling the Gaussian function from $-1$ to $1$
at regular intervals in each spatial direction, with the center of the filter
being at $G(0, 0, 0)$.
As the Gaussian function is a continuous function and $\int_\infty^\infty G(x)\,
\mathrm{d}x = 1$, the filter must be normalized by dividing
by the sum of the sampled points, or the downscaled image will become darker than
the original. Figure \ref{fig:downsample_gaussian} show the result of downscaling
a PRESIOUS stone with a Gaussian filter. The Gaussian filter preserves
the noise in the original dataset much better than the averaging filter.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{images/downsampling/gaussian.png}
	\caption{Elefsis Large 1 downscaled with a 5x5x5 Gaussian filter with $\sigma = 0.25$.}
	\label{fig:downsample_gaussian}
\end{figure}

\subsection{Comparison}

To compare which downscaling filter produces the best results when used for
phase one, a transformation is applied to a PRESIOUS dataset, and then
the transformed dataset is registered with the original. This is repeated for
several different transformations. Table \ref{tab:downsampling_comparison_1},
\ref{tab:downsampling_comparison_2} and \ref{tab:downsampling_comparison_3}
show the results of the comparison. The dataset used for this comparison is
\emph{Nidaros Good Large 1}. The size of the downscaling filter is $7x7x7$. The
Gaussian filter is created with $\sigma = 0.25$. The maximum number of iterations
is set to 10. The box filter and Gaussian filter columns list the rotations
in order as registered by the phase-correlation implementation.

\subsubsection{Data}

\begin{table}[H]
	\begin{center}
	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		30\degree, \emph{x}-axis & 34.1\degree, \emph{x}-axis & 34.1\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis  \\
		 & 356\degree, \emph{x}-axis & 356\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		\hline
	\end{tabularx}
	
	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		60\degree, \emph{x}-axis & 56.6\degree, \emph{x}-axis & 56.6\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 3.73\degree, \emph{x}-axis & 3.73\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		\hline
	\end{tabularx}
	
	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		90\degree, \emph{x}-axis & 89.7\degree, \emph{x}-axis & 89.7\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 1.37\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & \\
		 & 0\degree, \emph{z}-axis & \\
		 & 358\degree, \emph{x}-axis & \\
		 & 0\degree, \emph{y}-axis & \\
		 & 0\degree, \emph{z}-axis & \\
		 & 1.37\degree, \emph{x}-axis & \\
		\hline
	\end{tabularx}
	\end{center}
	\caption{First table with the results from registering with different re-sampling
	methods for downscaling with a 7x7x7 filter. The results are rounded to 3 significant
	figures.}
	\label{tab:downsampling_comparison_1}
\end{table}

\begin{table}[H]
	\begin{center}
	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		30\degree, \emph{y}-axis & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 33.1\degree, \emph{y}-axis & 33.1\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 357\degree, \emph{y}-axis & 357\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		\hline
	\end{tabularx}

	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		60\degree, \emph{y}-axis & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 153\degree, \emph{y}-axis & 153\degree, \emph{y}-axis \\
		 & 89.7\degree, \emph{z}-axis & 89.7\degree, \emph{z}-axis \\
		 & 126\degree, \emph{x}-axis & 306\degree, \emph{x}-axis \\
		 & 243\degree, \emph{y}-axis & 243\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 93.2\degree, \emph{x}-axis & 183\degree, \emph{x}-axis \\
		 & 213\degree, \emph{y}-axis & 60\degree, \emph{y}-axis \\
		 & 14.2\degree, \emph{z}-axis & 182\degree, \emph{z}-axis \\
		 & 28.2\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		\hline
	\end{tabularx}

	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		90\degree, \emph{y}-axis & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 89.7\degree, \emph{y}-axis & 89.7\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		\hline
	\end{tabularx}
	\end{center}
	\caption{Second table with the results from registering with different re-sampling
	methods for downscaling with a 7x7x7 filter. The results are rounded to 3 significant
	figures.}
	\label{tab:downsampling_comparison_2}
\end{table}

\begin{table}[H]
	\begin{center}
	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		30\degree, \emph{z}-axis & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 28.8\degree, \emph{z}-axis & 28.8\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 1.59\degree, \emph{z}-axis & 1.59\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		\hline
	\end{tabularx}

	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		60\degree, \emph{z}-axis & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 59.1\degree, \emph{z}-axis & 57.9\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 0\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0.789\degree, \emph{z}-axis & 2.36\degree, \emph{z}-axis \\
		 &  & 0\degree, \emph{x}-axis \\
		 &  & 0\degree, \emph{y}-axis \\
		 &  & 0\degree, \emph{z}-axis \\
		\hline
	\end{tabularx}

	\begin{tabularx}{\textwidth}{| X | X | X |}
		\hline
		\textbf{Ground truth} & \textbf{Box filter} & \textbf{Gaussian filter} \\
		\hline
		90\degree, \emph{z}-axis & 357\degree, \emph{x}-axis & 357\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 5.52\degree, \emph{x}-axis & 5.52\degree, \emph{x}-axis \\
		 & 0\degree, \emph{y}-axis & 0\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 0\degree, \emph{z}-axis \\
		 & 356\degree, \emph{x}-axis & 356\degree, \emph{x}-axis \\
		 & 1.38\degree, \emph{y}-axis & 1.38\degree, \emph{y}-axis \\
		 & 0\degree, \emph{z}-axis & 90.0\degree, \emph{z}-axis \\
		 & 0\degree, \emph{x}-axis & 2.70\degree, \emph{x}-axis \\
		\hline
	\end{tabularx}
	\end{center}
	\caption{Third table with the results from registering with different re-sampling
	methods for downscaling with a 7x7x7 filter. The results are rounded to 3 significant
	figures.}
	\label{tab:downsampling_comparison_3}
\end{table}

\subsection{Conclusion}

As apparent from table \ref{tab:downsampling_comparison_1}, \ref{tab:downsampling_comparison_2} 
and \ref{tab:downsampling_comparison_3} there are not much difference in the results from using
a box filter, or a Gaussian filter. However the Gaussian filter does give better
results when the dataset is rotated 90 degrees around the $z$-axis. The Gaussian
filter is therefore used in the final implementation.
