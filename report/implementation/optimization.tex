\section{Optimization method}
\label{sec:impl_optimization}

This section will discuss the two optimization methods considered for the
registration. These methods are hill climbing and gradient descent. These
methods are evaluated for accuracy and performance and then compared.

\subsection{Hill climbing}

Hill climbing is a local search optimization method. It is an iterative
method that improves an initial guess to a multi-variable function by changing a
single variable at a time. The implemented hill climbing algorithm does steepest
ascent hill climbing, where the function value of all candidate solutions are
calculated and the best solution is selected for the next iteration.

\begin{algorithm}
	\begin{algorithmic}
		\Function{HillClimbing}{$func, x, dx$}
			\State $best \gets \Call{func}{x}$
			\Repeat
				\State $candidates \gets \Call{Candidates}{x, dx}$
				\ForAll{$candidates$}
					\If{$\Call{func}{candidate} > best$}
						\State $best \gets \Call{func}{candidate}$
						\State $x \gets candidate$
					\EndIf
				\EndFor
			\Until{$Convergence$}
		\EndFunction
	\end{algorithmic}
	\caption{Hill climbing algorithm.}
	\label{alg:hillcimbing}
\end{algorithm}

\subsubsection{Implementation detail}

Hill climbing normally uses fixed steps. The version implemented in this thesis
reduces the step size by half every time an iteration doesn't find any improvements.
This done to improve the accuracy of the method.

\subsubsection{Convergence}

Hill climbing is not guaranteed to converge to the global maximum, but may get
stuck in a local maximum. The cross correlation function is not guaranteed to
have only one global maximum, the hope is that after the first registration
phase the initial guess will be close enough to the optimal solution to avoid
getting stuck in local maximums. Hill climbing is limited to traverse the
terrain by adjusting one variable at a time, making steep ridges that does not
align with the axes of the function hard to optimize.

The implementation of the hill climbing algorithm stops if a maximum number of
iterations is reached, or if a component of the $dx$ vector is lower than some
tolerance value.

\subsection{Gradient descent}

The gradient descent method is an optimization method that traverses the function
in the direction of the function's gradient until a local minimum is located.
Given a function $F(\mathbf{x})$, where $\mathbf{x}$ is a vector, the gradient
descent method iteratively improves an initial guess $\mathbf{x_0}$ until some
convergence criteria is met.

\begin{equation*}
	\mathbf{x_{n+1}} = \mathbf{x_{n}} - \gamma_n \nabla F(\mathbf{x_{n}})
\end{equation*}

$\gamma_n$ is the step size and determines the traversal distance for each step.
When solving a maximization problem the value of $\gamma_n$ is negative to
invert the step direction.

\subsubsection{Convergence}

For the gradient descent method to be guaranteed to converge to a global minimum,
the function $F$ has to be convex, or in other words have only one minimum.
In general the cross correlation function is not guaranteed to be convex. However
after the first phase of the registration method, the estimated transformation
will hopefully be within a convex area belonging to the global optimal solution.

The implementation of the gradient descent algorithm stops if a maximum number of
iterations is reached, or if the infinity norm of the gradient vector is below
some toleratance value.

\begin{equation*}
	Norm_{Inf}(\mathbf{x}) = max(abs(x))
\end{equation*}


\subsubsection{Gradient function}

The gradient of the function $F$ is defined as

\begin{equation*}
	\nabla F(\mathbf{x}) = 
		\Big( 
			\frac{\partial}{\partial x_0} F(\mathbf{x}), 
			\frac{\partial}{\partial x_1} F(\mathbf{x}),
			\cdots,
			\frac{\partial}{\partial x_{n-1}} F(\mathbf{x})
		\Big)^T
\end{equation*}

where $\frac{\partial}{\partial x_i} F(\mathbf{x})$ is the partial derivative
with respect to $x_i$. The partial derivative of the cross-correlation function
with respect to the transformations cannot be calculated directly. Instead the
partial derivative is approximated using a finite difference method. The finite
difference method used is the central difference.

\begin{equation*}
	\frac{\partial}{\partial x_{i}} F(\mathbf{x}) \approx \frac{F(\mathbf{x + h_i}) - F(\mathbf{x - h_i})}{2h}
\end{equation*}

$\mathbf{h_i}$ is a vector where all elements except the $i$th are equal to 0. The $i$th
element has the value $h$.

\subsection{Comparison}

When comparing the optimization methods, the dataset is registered with itself as
the target. In this case the null vector is the optimal solution. To compare the
methods, their performance and accuracy when the initial guess is different
from the null vector is compared. The number of iterations required by each
method to converge is used to compare the performance of the method. This is
because both the hill climbing algorithm and gradient descent method evaluate
the cross-correlation function 12 times per iteration, and evaluating
the cross-correlation function is the bottleneck of both methods. Table
\ref{tab:optimization_comparison} shows the results of the comparison.

\subsubsection{Parameters}

The datasets used in the comparison are downscaled with a $5x5x5$ averaging filter
and the maximum number of iterations is set to 50. Hill climbing uses
$dx = (4, 4, 4, 1, 1, 1)$ as the step vector, and $tolerance = 0.001$.
Gradient descent uses $\gamma = -10.0$ and $tolerance = 10^{-5}$.
The dataset used is \emph{Nidaros Good Large 1}.

\subsubsection{Data}

\begin{table}[H]
	\begin{center}
	\begin{tabularx}{\textwidth}{| l || X |}
		\hline
		Num & \multicolumn{1}{|c|}{\textbf{Initial guess}} \\
		\hline \hline
		0 & $\begin{pmatrix}20 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ \\
		\hline
		1 & $\begin{pmatrix}35 & -20 & 0 & 0 & 0 & 0\end{pmatrix}$ \\
		\hline
		2 & $\begin{pmatrix}30 & -15 & 40 & 0 & 0 & 0\end{pmatrix}$ \\
		\hline
		3 & $\begin{pmatrix}0 & 0 & 0 & 1 & 0 & 0\end{pmatrix}$ \\
		\hline
		4 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0.5 & -1\end{pmatrix}$ \\
		\hline
		5 & $\begin{pmatrix}0 & 0 & 0 & -1 & 1 & 0.7\end{pmatrix}$ \\
		\hline
		6 & $\begin{pmatrix}38 & 0 & 0 & 0 & 0.8 & 0\end{pmatrix}$ \\
		\hline
		7 & $\begin{pmatrix}-35 & 40 & 0 & -1.1 & 0 & 1.2\end{pmatrix}$ \\
		\hline
		8 & $\begin{pmatrix}33 & -33 & 39 & 1.15 & -0.7 & 0.8\end{pmatrix}$ \\
		\hline
	\end{tabularx}
	
	\begin{tabularx}{\textwidth}{| l || X | l |}
		\hline
		 & \multicolumn{2}{|c|}{\textbf{Hill climbing}} \\
		\hline \hline
		Num & Solution & Iterations \\

		\hline \hline
		0 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ & 14 \\
		\hline
		1 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ & 25 \\
		\hline
		2 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ & 33 \\
		\hline
		3 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ & 10 \\
		\hline
		4 & $\begin{pmatrix}0 & 0 & 0 & 0 & 0 & 0\end{pmatrix}$ & 12 \\
		\hline
		5 & $\begin{pmatrix}0 & 0.0156 & 0 & 0 & 0 & -0.00313\end{pmatrix}$ & 20 \\
		\hline
		6 & $\begin{pmatrix}0 & 0 & 0.0156 & 0 & -0.0164 & 0\end{pmatrix}$ & 25 \\
		\hline
		7 &  $\begin{pmatrix}0 & 0.0313 & 0 & -0.0141 & 0 &-0.00117\end{pmatrix}$ & 43 \\
		\hline
		8 & $\begin{pmatrix}0 & 0.125 & 0 & -0.00625 & -0.0125 & -0.0125\end{pmatrix}$ & 50 \\
		\hline
	\end{tabularx}
	
	\begin{tabularx}{\textwidth}{| l || X | l |}
		\hline
		 & \multicolumn{2}{|c|}{\textbf{Gradient descent}} \\
		\hline \hline
		Num & Solution & Iterations \\
		\hline \hline
		0 & $\begin{pmatrix}19.2 & 0.00984 & -0.000256 & -0.00401 & -0.0389 & -0.0786\end{pmatrix}$ & 50 \\
		\hline
		1 & $\begin{pmatrix}34.1 & -19.4 & -0.0444 & -0.0143 & -0.0705 & -0.158\end{pmatrix}$ & 50 \\
		\hline
		2 & $\begin{pmatrix}29.3 & -14.6 & 37.9 & -0.0544 & -0.342 & -0.222\end{pmatrix}$ & 50 \\
		\hline
		3 & $\begin{pmatrix}0.000148 & 0.0119 & -0.000238 & 0.175 & -0.00729 & -0.00222\end{pmatrix}$ & 50 \\
		\hline
		4 & $\begin{pmatrix}0.00105 & 0.00906 & -0.00399 & 0.000563 & 0.125 & -0.263\end{pmatrix}$ & 50 \\
		\hline
		5 &  $\begin{pmatrix}-0.0149 & -0.00190 & -0.00637 & -0.238 & 0.305 & 0.197\end{pmatrix}$ & 50 \\
		\hline
		6 & $\begin{pmatrix}37.0 & 0.00764 & -0.0566 & -0.00570 & 0.292 & -0.0537\end{pmatrix}$ & 50 \\
		\hline
		7 & $\begin{pmatrix}-34.1 & 39.3 & 0.0507 & -0.462 & 0.0228 & 1.39\end{pmatrix}$ & 50 \\
		\hline
		8 & $\begin{pmatrix} 32.2 & -32.3 & 37.0 & 0.869 & -1.14 & 0.683\end{pmatrix}$ & 50 \\
		\hline
	\end{tabularx}
	
	\end{center}
	\caption{Comparisons of the results from using different optimization
	methods for the second registration phase. The solutions are rounded to 3 significant figures.}
	\label{tab:optimization_comparison}
\end{table}

\subsection{Conclusion}

The optimization method chosen for phase two is hill climbing as it converges much
faster than the gradient descent and gives more accurate results. Gradient descent converges slowly because the cross-
correlation function is almost flat. Using a larger value for $\gamma$ is not viable
as that will cause even larger errors in the rotation components.
