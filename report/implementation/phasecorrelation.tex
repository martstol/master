\section{Phase One: 3D phase correlation}
\label{sec:impl_pc}

Extending the phase correlation method from two dimensions to three
dimensions is a simple task as the Fourier shift theorem applies in three
dimensions. Given a function $f_1(x, y, z)$ and $f_2(x, y, z)$ where
$f_2(x, y, z) = f_1(x - x_0, y - y_0, z - z_0)$. The Fourier transforms of $f_1$
and $f_2$ are $F_1$ and $F_2$ are then related by the following equation:

\begin{equation*}
	F_2(u, v, w) = e^{-i 2 \pi (u x_0 + v y_0 + w z_0)} F_1(u, v, w)
\end{equation*}

Then following the approach outlined in the background chapter to calculate the
phase correlation function.

This is the pseudo-code for the phase correlation algorithm:
\begin{algorithm}
	\begin{algorithmic}
		\Function{PhaseCorrelation}{$f_1, f_2$}
			\State $F_1 \gets \Call{FFT}{f_1}$
			\State $F_2 \gets \Call{FFT}{f_2}$
			\State $C \gets \Call{CrossPowerSpectrum}{F_1, F_2}$
			\State \Return $\Call{IFFT}{C}$
		\EndFunction
	\end{algorithmic}
	\caption{Calculate the value of the phase correlation function.}
	\label{alg:phasecorr}
\end{algorithm}

\begin{algorithm}
	\begin{algorithmic}
		\Function{RegisterTranslation}{$f_1, f_2$}
			\State $C \gets \Call{PhaseCorrelation}{f_1, f_2}$
			\State $peak \gets \Call{FindMax}{C}$
			\State $x \gets peak.x$
			\State $y \gets peak.y$
			\State $z \gets peak.z$
			\If{$x > f_1.width / 2$}
			\State $x \gets x - f_1.width$
			\EndIf
			\If{$y > f_1.height / 2$}
			\State $y \gets y - f_1.height$
			\EndIf
			\If{$z > f_1.depth / 2$}
			\State $z \gets z - f_1.depth$
			\EndIf
			\State \Return $(x, y, z)$
		\EndFunction
	\end{algorithmic}
	\caption{Register the angle of rotation around a given axis.}
	\label{alg:registertranslation}
\end{algorithm}

\subsection{Applied to rotations in three dimensions}

When working with three-dimensional data the rotation can be around any of the
three possible axes and any combination of them. There are no mappings that
convert rotations around unknown axes in three dimensions to translations. This
is not an issue in two dimensions as there is only one possible axis of rotation.
First the method for estimating rotation around a known axis in three dimensions
will be outlined, then a method for estimating rotations around multiple axes
is described.

\subsubsection{Rotation with known axis}

The following is an explanation of the method when applied to rotations around
the $z$-axis. Given two three- dimensional functions $f_1(x, y, z)$ and
$f_2(x, y, z)$, where $f_2$ is identical to $f_1$ rotated by $\alpha$ around
the $z$-axis and translated by $(x_0, y_0, z_0)$.

\begin{equation*}
	f_2(x, y, z)
	= f_1(x\cos\alpha - y\sin\alpha - x_0, x\sin\alpha + y\cos\alpha - y_0, z - z_0)
\end{equation*}

The Fourier transforms of $f_1$ and $f_2$ are $F_1$ and $F_2$ which are related
by the following equation:
\begin{equation*}
	F_2(u, v, w) = e^{-i 2 \pi (u x_0 + v y_0 + w z_0)}
	F_1(u\cos\alpha - v\sin\alpha, u\sin\alpha + v\cos\alpha, w)
\end{equation*}

To register the rotation, the method described in section \ref{sec:imgreg} is
applied. However when working with three-dimensional data, the function has to be
converted to cylindrical coordinates. This process is described in section
\ref{sec:cylindrical}.

\begin{equation*}
	F_{2 p}(\rho, \theta, h) = F_{1 p}(\rho, \theta - \alpha, h)
\end{equation*}

Next algorithm \ref{alg:phasecorr} is applied to $F_{1 p}$ and $F_{2 p}$. The
value of alpha is determined by the second coordinate ($y$ coordinate) of the
peak of the phase correlation function.

\begin{algorithm}
	\begin{algorithmic}
		\Function{RegisterRotation}{$f_1, f_2, axis$}
			\State $F_1 \gets \Call{FFT}{f_1}$
			\State $F_2 \gets \Call{FFT}{f_2}$
			\State $A_1 \gets \Call{abs}{F_1}$
			\State $A_2 \gets \Call{abs}{F_2}$
			\State $P_1 \gets \Call{CylindricalCoords}{A_1, axis}$
			\State $P_2 \gets \Call{CylindricalCoords}{A_2, axis}$
			\State $C \gets \Call{PhaseCorrelation}{P_1, P_2}$
			\State $Peak \gets \Call{FindMax}{C}$
			\State \Return $\frac{Peak.y}{f_1.height} \times 2\pi$
		\EndFunction
	\end{algorithmic}
	\caption{Register the angle of rotation around a given axis.}
	\label{alg:registerrotation}
\end{algorithm}

\subsubsection{Rotation with unknown axis}

The approach used in this thesis for registering three dimensional rotations
follows the scheme proposed by \citeauthor{5656976} in \cite{5656976}. They
register rotation around the $x$, $y$ and $z$-axis independently, starting with
the $z$-axis, then the $y$-axis and finally the $x$-axis. After the rotation
around an axis has been registered, the inverse transformation is applied before
the next registration. This process repeats until the estimated angle of
rotation around each axis converges to 0. Finally the translation is estimated
using phase correlation. This is the basis for the pseudo code for the
\emph{PhaseOne} function in algorithm \ref{alg:vdr}. This method is not guaranteed
to converge to the global optimum of the similarity metric\cite{Bican:2009:RRC:1552570.1552693}
