\section{Converting to cylindrical coordinates}
\label{sec:cylindrical}

To register the rotation of the datasets with phase correlation, it is
necessary to express the rotation as a translation. This is done by converting
the datasets to cylindrical coordinates, where a rotation can be expressed as
a translation of the angular coordinate. Cylindrical coordinates are necessary to
specify every three dimensional point in the datasets.

Cylindrical coordinates specify the position of a point as the distance from a
reference axis, the direction from the axis relative to a reference direction
and the distance from a reference plane perpendicular to the reference axis. The
distance from the reference axis is called the radial coordinate $\rho$, the
direction is called the angular coordinate $\theta$ and the distance from the
reference plane is called the height $h$. This is illustrated in figure
\ref{fig:cylindricalcoords}. Cylindrical coordinates are used to express the
rotation in the three dimensional Cartesian coordinate system as a translation
in the angular coordinate. The conversion differs depending on the direction of
the reference axis of the cylinder. The scheme for registering rotations in
three dimensions as described in section \ref{sec:impl_pc} independently registers
rotations around the $x$, $y$ and $z$-axis. Therefore methods for converting to
cylindrical coordinates where the reference axis of the cylinder corresponds to
the $x$, $y$ and $z$-axis are required.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{images/imreg/cylindrical_coordinates.png}
	\caption{Illustration of how the cylindrical coordinates $\rho$, $\theta$ and
	$h$ expresses a location in three dimensional space.}
	\label{fig:cylindricalcoords}
\end{figure}

\begin{description}
	\item[X-axis] \hfill \\
	$x = h$ \hfill \\
	$y = \rho\cos\theta$ \hfill \\
	$z = \rho\sin\theta$
	\item[Y-axis] \hfill \\
	$x = \rho\sin\theta$ \hfill \\
	$y = h$ \hfill \\
	$z = \rho\cos\theta$
	\item[Z-axis] \hfill \\
	$x = \rho\cos\theta$ \hfill \\
	$y = \rho\sin\theta$ \hfill \\
	$z = h$
\end{description}

The Cartesian
coordinates obtained from the equations above seldom correspond to an exact
voxel in the original dataset, but is situated between several voxels. It
is necessary to use a method for sampling the original dataset, the method chosen
is trilinear interpolation which is described in section \ref{sec:interpolation}.

The accuracy of the phase-correlation is highly tied to the resolution of the
cylindrical coordinates dataset. If the resolution of the angular coordinate is
low, the accuracy of the registered angle limited. The size of the angular
dimension is therefore always at least $360$. The height of the cylindrical
coordinates dataset is the same as the size of the dimension corresponding to the
reference axis. The radius is the distance from the reference
axis going through the center of the original dataset to the edge of the dataset.
