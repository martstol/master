\section{Voxel Datasets}
\label{sec:voxel_datasets}

The purpose of this thesis is to develop a method for registering voxel
datasets for the PRESIOUS project. These datasets are created by X-ray CT
(Computed Tomography) of the PRESIOUS stones, where several X-ray images from
multiple angles are processed to create a three dimensional image of the
scanned object.

The size of the PRESIOUS stones vary, the Elefsis stones have dimensions of around
$20x20x20 mm^3$: The Nidaros stones measure roughly $50x50x20 mm^3$.
Each stone has two drill holes. The aim of these holes is to create easy to
detect features to make the registration process easier.

For each stone there are two datasets, called \emph{round 1} and \emph{round 2}.
The difference between each round is that the stone has been placed inside a
Accelerated Erosion Chamber. In this thesis a registration
method for aligning the datasets of the same stone is developed. This will
make it possible to identify the changes in the stone's surface due to erosion.

All the datasets have a high presence of Gaussian noise, and
changes in the stones between the scans are expected as the stones erode. These
factors complicate the registration process, it is important to select a method
that is noise tolerant. The possible transformations between the datasets are
translations and rotations. The range of possible rotations are 0 to 360 degrees
around any axis in three dimensions.

The employed registration method has to be suitable for mono-modal, temporal
registration as all datasets are created using x-ray CT at different points
in time with expected changes in the datasets between each scan.

\subsection{File storage}

Each dataset is stored as a set of multiple dicom
\footnote{\url{http://medical.nema.org/standard.html}} files, where each file is
a slice of the volume. The total size of an
entire voxel dataset for this thesis is typically around 6 to 10 gigabytes with
a resolution of up to 2000 voxels in each spatial direction. Each individual
voxel stores a single intensity value encoded as a 16-bits unsigned integer.
The large size of the datasets is an additional factor to consider when developing
the registration method. Storing both datasets in main memory on most normal desktop
computers is currently infeasible and even a $O(n \log n)$ similarity metric will be
very slow.

\subsection{Overview}

Table \ref{tab:datasets} gives an overview of the datasets used in this thesis
and their resolution (number of voxels in each dimension). Visual examples
of all the datasets can be found in Appendix \ref{appendix:datasets}. The name
column is the label of each stone, while the round 1 and round 2 columns give
the dimensions of each round's dataset.

\begin{table}[H]
	\begin{center}
	\begin{tabular}{| l || l | l | l || l | l | l |}
		\hline
		 & \multicolumn{3}{|c|}{\textbf{Round 1}} & \multicolumn{3}{|c|}{\textbf{Round 2}} \\
		\hline \hline
		Name & Width & Height & Depth & Width & Height & Depth \\
		\hline \hline
		Elefsis Large 1 & 1999 & 2000 & 1216 & 1634 & 1688 & 1162 \\
		\hline
		Elefsis Large 2 & 2000 & 1999 & 1185 & 1599 & 1530 & 1158 \\
		\hline
		Elefsis Small 1 & 1999 & 2000 & 1201 & 1606 & 1572 & 1139 \\
		\hline
		Nidaros Bad Large 1 & 1998 & 1877 & 917 & 1882 & 832 & 1863 \\
		\hline
		Nidaros Bad Large 3 & 1561 & 1566 & 666 & 1553 & 1572 & 654 \\
		\hline
		Nidaros Bad Small 1 & 1500 & 1561 & 1241 & 1630 & 1682 & 1272 \\
		\hline
		Nidaros Good Large 1 & 1557 & 1832 & 974 & 1622 & 998 & 1866 \\
		\hline
		Nidaros Good Large 2 & 1551 & 1841 & 949 & 768 & 1518 & 1881 \\
		\hline
		Nidaros Good Large 3 & 1764 & 1551 & 641 & 1832 & 1537 & 663 \\
		\hline
		Nidaros Good Small 1 & 1999 & 2000 & 1517 & 1595 & 1574 & 1471 \\
		\hline
	\end{tabular}
	\end{center}
	\caption{Table of the datasets used in this thesis. The width, height and depth
	columns give the resolutions of the datasets.}
	\label{tab:datasets}
\end{table}
